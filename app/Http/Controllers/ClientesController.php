<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ClientesController extends Controller
{
    public function index(Request $request)
    {
      $this->comprueba();
      $busqueda = $request->get('busquedaId');
      $clientes=DB::table('clientes')
      ->selectRaw('*,clientes.id as idcl')
      ->where('clientes.estado',1)
      ->where(function ($query) use ($busqueda) {
          $query->where('clientes.nombre','LIKE', "%$busqueda%")
          ->orWhere('rutas.ruta','LIKE', "%$busqueda%");
        })

      ->join('rutas','clientes.ruta_id','=','rutas.id')
      ->paginate(10);
      return view('clientes.index',compact('clientes'));
    }

    public function create()
    {
      return view('clientes.create');
    }

    public function edit($id)
    {
      $cliente=DB::table('clientes')
      ->selectRaw('*,clientes.id as idcl')
      ->where('clientes.id',$id)
      ->join('rutas','clientes.ruta_id','=','rutas.id')
      ->first();
      return view('clientes.edit',compact('cliente'));
    }

    public function update(Request $request, $id)
    {
      DB::table('clientes')
      ->where('id',$id)->update(
          [
            'nombre' => $request->nombre,

            'direccion'=> $request->direccion,
            'negocio' => $request->negocio,
            'colonia' => $request->colonia,
            'poblacion' => $request->poblacion,
            'telefono' => $request->telefono,
            'correo' => $request->correo,
            'ruta_id' => $request->ruta_id
          ]
      );
      return redirect()->route('clientes.index')->with('info','Se actualizó correctamente');
    }
    public function store(Request $request)
    {
      DB::table('clientes')
      ->insert(
          [
            'nombre' => $request->nombre,

            'direccion'=> $request->direccion,
            'negocio' => $request->negocio,
            'colonia' => $request->colonia,
            'poblacion' => $request->poblacion,
            'telefono' => $request->telefono,
            'correo' => $request->correo,
            'ruta_id' => $request->ruta_id
          ]
      );
      return redirect()->route('clientes.index')->with('info','Se guardó correctamente');
    }
    public function destroy(Request $request, $id)
    {
      DB::table('clientes')
      ->where('id',$id)->update(
          [
            'estado' => 2
          ]
      );
      return redirect()->route('clientes.index')->with('info','Se eliminó correctamente');
    }

    public function comprueba(){
      $remisiones=DB::table('remisiones')
      ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre,clientes.id as idcli')
      ->where('remisiones.estado',1)
      ->join('clientes','remisiones.id_cliente','=','clientes.id')
      ->get();
      $pagos=DB::table('pagos')
      ->where('estado',1)
      ->orderBy('fecha','asc')
      ->get();
      $deudores = [];
      foreach($remisiones as $r){
        $cont=0;
        $ultimop="";
        foreach($pagos as $p){
          if($p->id_remision==$r->idre){
            $cont=$cont+$p->cantidad;
            $ultimop=$p->fecha;
          }
        }
        $ultimoPago = new \DateTime($ultimop);
        $hoy = new \DateTime("NOW");
        $hoy = new \DateTime($hoy->format("Y-m-d"));
        $fechaEstipulada = new \DateTime($r->fechafin);
        //$diff = $utlimoPago->diff($fechaEstipulada);
        if($cont>=$r->importe){
          //ya está pagado
          //si el último pago es antes de cumplir la fecha estipulada pasa a Completado
          if($ultimoPago<=$fechaEstipulada){
            DB::table('remisiones')
            ->where('id',$r->idre)->update(
                [
                  'pago' => 1
                ]
            );
            //si no, pasa a completado a destiempo
          }else{
            DB::table('remisiones')
            ->where('id',$r->idre)->update(
                [
                  'pago' => 2
                ]
            );
          }
        }else{
          //no está pagado
          //si aún quedan días para efectuar su pago, no hacer nada
          if($hoy<=$fechaEstipulada){
            DB::table('remisiones')
            ->where('id',$r->idre)->update(
                [
                  'pago' => 3
                ]
            );
            //si no quedan días, el cliente pasa a deudor en el array
          }else{
            array_push($deudores,$r->id_cliente);
            DB::table('remisiones')
            ->where('id',$r->idre)->update(
                [
                  'pago' => 4
                ]
            );
          }
        }
      }
      DB::table('clientes')
      ->update(
          [
            'paga' => 1
          ]
      );

      //ejecutamos a los clientes deudores
      /*foreach ($deudores as $d) {
        DB::table('clientes')
        ->where('id',$d)->update(
            [
              'paga' => 2
            ]
        );
      }*/
    }
}
