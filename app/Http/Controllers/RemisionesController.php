<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use NumerosEnLetras;

class RemisionesController extends Controller
{
    public function create()
    {
      return view('remisiones.crearremision');
    }

    public function index(Request $request){
      app('App\Http\Controllers\ClientesController')->comprueba();
      $busqueda = $request->get('busquedaId');
      $remisiones=DB::table('remisiones')
      ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre')
      ->where('clientes.nombre','LIKE', "%$busqueda%")
      ->where('remisiones.estado',1)
      ->join('clientes','remisiones.id_cliente','=','clientes.id')
      ->orderBy('remisiones.id','desc')
      ->paginate(10);
      $pagos=DB::table('pagos')
      ->where('estado',1)
      ->orderBy('fecha','desc')
      ->get();
      return view('remisiones.index',compact('remisiones','pagos'));
    }

    public function store(Request $request)
    {
      if($request->importeTotal==null){
        $request->importeTotal=0;
      }

      $id = DB::table('remisiones')->insertGetId(
          [
           'id_cliente' => $request->id,
           'importe' => $request->importeTotal,
           'fecha' => $request->fecha,
           'estado' => 1,
           'fechafin' => $request->fechafin,
          ]
      );

      for($i = 1; $i < 25; $i++){
        DB::table('conceptos')->insert(
            [
             'cantidad' => $request["cantidad$i"],
             'descripcion' => $request["descripcion$i"],
             'precio' => $request["precio$i"],
             'iva' => $request["iva$i"],
             'importe' => $request["importe$i"],
             'id_remision' => $id,
            ]
        );
      }
      app('App\Http\Controllers\ClientesController')->comprueba();
      return redirect()->route('remisiones.index')->with('info','Se guardó correctamente');
    }

    public function update(Request $request, $id)
    {
      app('App\Http\Controllers\ClientesController')->comprueba();
      if($request->importeTotal==null){
        $request->importeTotal=0;
      }
      DB::table('remisiones')
      ->where('id',$id)->update(
          [
           'id_cliente' => $request->id,
           'importe' => $request->importeTotal,
           'estado' => 1,
           'fecha' => $request->fecha,
           'fechafin' => $request->fechafin,
          ]
      );

      for($i = 1; $i < 25; $i++){
        DB::table('conceptos')
        ->where('id',$request["id$i"])->update(
            [
             'cantidad' => $request["cantidad$i"],
             'descripcion' => $request["descripcion$i"],
             'precio' => $request["precio$i"],
             'iva' => $request["iva$i"],
             'importe' => $request["importe$i"],
             'id_remision' => $id,
            ]
        );
      }
      app('App\Http\Controllers\ClientesController')->comprueba();
      return redirect()->route('remisiones.index')->with('info','Se actualizó correctamente');
    }

    public function descripcionAs(Request $request)
    {
      $pal=strtoupper($request->term);

       $descripcion=DB::table('catalogo')
       ->selectRaw('catalogo.nombre')
       ->where('catalogo.estado',1)
       ->where(function ($query) use ($pal) {
           $query->where('catalogo.nombre','LIKE', "%$pal%")
           ->orWhere('catalogo.descripcion','LIKE', "%$pal%")
           ->orWhere('catalogo.codigo','LIKE', "%$pal%")
           ->orWhere('catalogo.sustancia','LIKE', "%$pal%")
           ->orWhere('catalogo.indice','LIKE', "%$pal%")
           ->orWhere('catalogo.oferta','LIKE', "%$pal%")
           ->orWhere('laboratorios.laboratorio','LIKE', "%$pal%");
       })
       ->join('laboratorios','catalogo.id_laboratorio','=','laboratorios.id')
       ->limit(10)
       ->get();


      foreach ($descripcion as $c) {
         $rDescripcion[]=[
           'nombre'=>$c->nombre,
           'label'=>$c->nombre
         ];

      }
      return Response($rDescripcion);
    }

    public function destroy($id){
      DB::table('remisiones')
      ->where('id',$id)->update(
          [
           'estado' => 2
          ]
      );
      return redirect()->route('remisiones.index')->with('info','Se eliminó correctamente');
    }

    public function edit($id){
      $conceptos=DB::table('conceptos')
      ->where('id_remision',$id)
      ->get();

      $remision=DB::table('remisiones')
      ->selectRaw('*,remisiones.id as idre')
      ->join('clientes','remisiones.id_cliente','=','clientes.id')
      ->where('remisiones.id',$id)
      ->first();
      return view('remisiones.edit',compact('remision','conceptos'));
    }

    public function show($id){
      $conceptos=DB::table('conceptos')
      ->where('id_remision',$id)
      ->get();

      $remision=DB::table('remisiones')
      ->selectRaw('*,remisiones.id as idre')
      ->join('clientes','remisiones.id_cliente','=','clientes.id')
      ->where('remisiones.id',$id)
      ->first();
      $letras= strtoupper(NumerosEnLetras::convertir($remision->importe,'M.N.',true));
      return view('remisiones.show',compact('remision','conceptos','letras'));
    }
}
