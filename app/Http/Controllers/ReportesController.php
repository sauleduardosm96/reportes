<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReportesController extends Controller
{
    public function reporteClientes(Request $request)
    {
      $meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO",
  									 "AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");
      $fecha2=explode("-",$request->fecha);
      if($fecha2[1]<'10'){
      	$fecha2[1]=substr($fecha2[1],1);
      }elseif($fecha2[1]>'9'){
      	$fecha2[1];
      }
      $letras=$fecha2[2]." DE ".$meses[$fecha2[1]-1]." DE ".$fecha2[0];


      $fecha4=explode("-",$request->fechafin);
      if($fecha4[1]<'10'){
      	$fecha4[1]=substr($fecha4[1],1);
      }elseif($fecha4[1]>'9'){
      	$fecha2[1];
      }
      $letras2=$fecha4[2]." DE ".$meses[$fecha4[1]-1]." DE ".$fecha4[0];
      $texto = "REPORTE DE SALDOS DEL ".$letras." AL ".$letras2;
      $pagos=DB::table('pagos')
      ->where('estado',1)
      ->orderBy('fecha','desc')
      ->get();
      if($request->id!=null){
        if($request->estado==5){

          $remisionesAlt=DB::table('remisiones')
          ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre')
          ->where('remisiones.fecha','<',$request->fecha)
          ->where('remisiones.id_cliente',$request->id)
          ->join('clientes','remisiones.id_cliente','=','clientes.id')
          ->where('remisiones.estado',1)
          ->get();

          $remisiones=DB::table('remisiones')
          ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre')
          ->whereBetween('remisiones.fecha',[$request->fecha,$request->fechafin])
          ->where('remisiones.id_cliente',$request->id)
          ->join('clientes','remisiones.id_cliente','=','clientes.id')
          ->where('remisiones.estado',1)
          ->get();
          $cliente = DB::table('clientes')->where('id',$request->id)->first();
          return view('reportes.index',compact('remisiones','pagos','texto','cliente','remisionesAlt'));
        }else{
          $remisiones=DB::table('remisiones')
          ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre')
          ->where('remisiones.pago',$request->estado)
          ->whereBetween('remisiones.fecha',[$request->fecha,$request->fechafin])
          ->where('remisiones.id_cliente',$request->id)
          ->join('clientes','remisiones.id_cliente','=','clientes.id')
          ->where('remisiones.estado',1)
          ->get();
          $cliente = DB::table('clientes')->where('id',$request->id)->first();
          return view('reportes.index',compact('remisiones','pagos','texto','cliente'));
        }
      }
      else{
        if($request->estado==5){

          $remisiones=DB::table('remisiones')
          ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre')
          ->whereBetween('remisiones.fecha',[$request->fecha,$request->fechafin])
          ->join('clientes','remisiones.id_cliente','=','clientes.id')
          ->where('remisiones.estado',1)
          ->get();
          return view('reportes.index',compact('remisiones','pagos','texto'));
        }else{
          $remisiones=DB::table('remisiones')
          ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre')
          ->where('remisiones.pago',$request->estado)
          ->whereBetween('remisiones.fecha',[$request->fecha,$request->fechafin])
          ->join('clientes','remisiones.id_cliente','=','clientes.id')
          ->where('remisiones.estado',1)
          ->get();
          return view('reportes.index',compact('remisiones','pagos','texto'));
        }
      }

    }
    public function reporteProveedores(Request $request)
    {
      $meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO",
                     "AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");
      $fecha2=explode("-",$request->fecha);
      if($fecha2[1]<'10'){
        $fecha2[1]=substr($fecha2[1],1);
      }elseif($fecha2[1]>'9'){
        $fecha2[1];
      }
      $letras=$fecha2[2]." DE ".$meses[$fecha2[1]-1]." DE ".$fecha2[0];


      $fecha4=explode("-",$request->fechafin);
      if($fecha4[1]<'10'){
        $fecha4[1]=substr($fecha4[1],1);
      }elseif($fecha4[1]>'9'){
        $fecha2[1];
      }
      $letras2=$fecha4[2]." DE ".$meses[$fecha4[1]-1]." DE ".$fecha4[0];
      $texto = "REPORTE DE SALDOS DEL ".$letras." AL ".$letras2;
      $abonos=DB::table('abonos')
      ->where('estado',1)
      ->orderBy('fecha','desc')
      ->get();
      if($request->id!=null){
        if($request->estado==5){

          $comprasAlt=DB::table('compras')
          ->selectRaw('compras.*,proveedores.*, compras.id as idre')
          ->where('compras.fecha','<',$request->fecha)
          ->where('compras.id_proveedor',$request->id)
          ->join('proveedores','compras.id_proveedor','=','proveedores.id')
          ->where('compras.estado',1)
          ->get();

          $compras=DB::table('compras')
          ->selectRaw('compras.*,proveedores.*, compras.id as idre')
          ->whereBetween('compras.fecha',[$request->fecha,$request->fechafin])
          ->where('compras.id_proveedor',$request->id)
          ->join('proveedores','compras.id_proveedor','=','proveedores.id')
          ->where('compras.estado',1)
          ->get();
          $proveedor = DB::table('proveedores')->where('id',$request->id)->first();
          return view('reportes.indexPro',compact('compras','abonos','texto','proveedor','comprasAlt'));
        }else{
          $compras=DB::table('compras')
          ->selectRaw('compras.*,proveedores.*, compras.id as idre')
          ->where('compras.cobro',$request->estado)
          ->whereBetween('compras.fecha',[$request->fecha,$request->fechafin])
          ->where('compras.id_proveedor',$request->id)
          ->join('proveedores','compras.id_proveedor','=','proveedores.id')
          ->where('compras.estado',1)
          ->get();
          $proveedor = DB::table('proveedores')->where('id',$request->id)->first();
          return view('reportes.indexPro',compact('compras','abonos','texto','proveedor'));
        }
      }
      else{
        if($request->estado==5){

          $compras=DB::table('compras')
          ->selectRaw('compras.*,proveedores.*, compras.id as idre')
          ->whereBetween('compras.fecha',[$request->fecha,$request->fechafin])
          ->join('proveedores','compras.id_proveedor','=','proveedores.id')
          ->where('compras.estado',1)
          ->get();
          return view('reportes.indexPro',compact('compras','abonos','texto'));
        }else{
          $compras=DB::table('compras')
          ->selectRaw('compras.*,proveedores.*, compras.id as idre')
          ->where('compras.cobro',$request->estado)
          ->whereBetween('compras.fecha',[$request->fecha,$request->fechafin])
          ->join('proveedores','compras.id_proveedor','=','proveedores.id')
          ->where('compras.estado',1)
          ->get();
          return view('reportes.indexPro',compact('compras','abonos','texto'));
        }
      }

    }
}
