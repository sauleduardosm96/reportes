<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PagosController extends Controller
{

    public function edit($id)
    {
      $pagos=DB::table('pagos')
      ->where('id_remision',$id)
      ->where('estado',1)
      ->orderBy('fecha','desc')
      ->get();
      $remision=DB::table('remisiones')
      ->where('remisiones.id',$id)
      ->selectRaw('remisiones.*,clientes.*, remisiones.id as idre')
      ->join('clientes','remisiones.id_cliente','=','clientes.id')
      ->first();
      return view('pagos.edit',compact('pagos','remision'));
    }

    public function update(Request $request, $id)
    {
      app('App\Http\Controllers\ClientesController')->comprueba();
      $pagos=DB::table('pagos')
      ->where('estado',1)
      ->where('id_remision',$id)
      ->orderBy('fecha','asc')
      ->get();

      $cont=0;
      foreach($pagos as $p){
        $cont=$cont+$p->cantidad;
      }
      $remision=DB::table('remisiones')
      ->where('id',$id)
      ->first();
      $restan=$remision->importe-$cont;
      if($request->cantidad>$restan){
        return redirect()->route('pagos.edit',$id)->with('info','la cantidad excede el restante');
      }else{
        DB::table('pagos')
        ->insert(
            [
              'cantidad' => $request->cantidad,
              'descripcion'=> $request->descripcion,
              'fecha' => $request->fecha,
              'id_remision' => $id,
            ]
        );
        return redirect()->route('pagos.edit',$id)->with('info','Se guardó correctamente');
      }

    }
    public function store(Request $request, $id)
    {

    }
    public function destroy(Request $request, $id)
    {
      DB::table('pagos')
      ->where('id',$id)->update(
          [
            'estado' => 2
          ]
      );
    return redirect()->route('pagos.edit',$request->val)->with('info','Se eliminó correctamente');
    }

}
