<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use NumerosEnLetras;

class ComprasController extends Controller
{
    public function create()
    {
      return view('compras.crearcompra');
    }

    public function index(Request $request){
      app('App\Http\Controllers\ProveedoresController')->comprueba();
      $busqueda = $request->get('busquedaId');
      $compras=DB::table('compras')
      ->selectRaw('compras.*,proveedores.*, compras.id as idre')
      ->where('proveedores.nombre','LIKE', "%$busqueda%")
      ->where('compras.estado',1)
      ->join('proveedores','compras.id_proveedor','=','proveedores.id')
      ->orderBy('compras.id','desc')
      ->paginate(10);
      $abonos=DB::table('abonos')
      ->where('estado',1)
      ->orderBy('fecha','desc')
      ->get();
      return view('compras.index',compact('compras','abonos'));
    }

    public function store(Request $request)
    {
      if($request->importeTotal==null){
        $request->importeTotal=0;
      }

      $id = DB::table('compras')->insertGetId(
          [
           'id_proveedor' => $request->id,
           'importe' => $request->importeTotal,
           'fecha' => $request->fecha,
           'estado' => 1,
           'fechafin' => $request->fechafin,
          ]
      );

      for($i = 1; $i < 25; $i++){
        DB::table('articulos')->insert(
            [
             'cantidad' => $request["cantidad$i"],
             'descripcion' => $request["descripcion$i"],
             'precio' => $request["precio$i"],
             'iva' => $request["iva$i"],
             'importe' => $request["importe$i"],
             'id_compra' => $id,
            ]
        );
      }
      app('App\Http\Controllers\ProveedoresController')->comprueba();
      return redirect()->route('compras.index')->with('info','Se guardó correctamente');
    }

    public function update(Request $request, $id)
    {
      app('App\Http\Controllers\ProveedoresController')->comprueba();
      if($request->importeTotal==null){
        $request->importeTotal=0;
      }
      DB::table('compras')
      ->where('id',$id)->update(
          [
           'id_proveedor' => $request->id,
           'importe' => $request->importeTotal,
           'estado' => 1,
           'fecha' => $request->fecha,
           'fechafin' => $request->fechafin,
          ]
      );

      for($i = 1; $i < 25; $i++){
        DB::table('articulos')
        ->where('id',$request["id$i"])->update(
            [
             'cantidad' => $request["cantidad$i"],
             'descripcion' => $request["descripcion$i"],
             'precio' => $request["precio$i"],
             'iva' => $request["iva$i"],
             'importe' => $request["importe$i"],
             'id_compra' => $id,
            ]
        );
      }
      app('App\Http\Controllers\ProveedoresController')->comprueba();
      return redirect()->route('compras.index')->with('info','Se actualizó correctamente');
    }

    public function descripcionAs(Request $request)
    {
      $pal=strtoupper($request->term);

       $descripcion=DB::table('catalogo')
       ->selectRaw('catalogo.nombre')
       ->where('catalogo.estado',1)
       ->where(function ($query) use ($pal) {
           $query->where('catalogo.nombre','LIKE', "%$pal%")
           ->orWhere('catalogo.descripcion','LIKE', "%$pal%")
           ->orWhere('catalogo.codigo','LIKE', "%$pal%")
           ->orWhere('catalogo.sustancia','LIKE', "%$pal%")
           ->orWhere('catalogo.indice','LIKE', "%$pal%")
           ->orWhere('catalogo.oferta','LIKE', "%$pal%")
           ->orWhere('laboratorios.laboratorio','LIKE', "%$pal%");
       })
       ->join('laboratorios','catalogo.id_laboratorio','=','laboratorios.id')
       ->limit(10)
       ->get();


      foreach ($descripcion as $c) {
         $rDescripcion[]=[
           'nombre'=>$c->nombre,
           'label'=>$c->nombre
         ];

      }
      return Response($rDescripcion);
    }

    public function destroy($id){
      DB::table('compras')
      ->where('id',$id)->update(
          [
           'estado' => 2
          ]
      );
      return redirect()->route('compras.index')->with('info','Se eliminó correctamente');
    }

    public function edit($id){
      $articulos=DB::table('articulos')
      ->where('id_compra',$id)
      ->get();

      $compra=DB::table('compras')
      ->selectRaw('*,compras.id as idre')
      ->join('proveedores','compras.id_proveedor','=','proveedores.id')
      ->where('compras.id',$id)
      ->first();
      return view('compras.edit',compact('compra','articulos'));
    }

    public function show($id){
      $articulos=DB::table('articulos')
      ->where('id_compra',$id)
      ->get();

      $compra=DB::table('compras')
      ->selectRaw('*,compras.id as idre')
      ->join('proveedores','compras.id_proveedor','=','proveedores.id')
      ->where('compras.id',$id)
      ->first();
      $letras= strtoupper(NumerosEnLetras::convertir($compra->importe,'M.N.',true));
      return view('compras.show',compact('compra','articulos','letras'));
    }
}
