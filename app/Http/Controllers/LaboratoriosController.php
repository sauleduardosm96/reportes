<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class LaboratoriosController extends Controller
{
    public function index(Request $request)
    {
      $busqueda = $request->get('busquedaId');
      $laboratorios=DB::table('laboratorios')
      ->where('laboratorio','LIKE', "%$busqueda%")
      ->where('estado',1)
      ->paginate(10);
      return view('laboratorios.index',compact('laboratorios'));
    }

    public function create()
    {
      return view('laboratorios.create');
    }

    public function edit($id)
    {
      $laboratorio=DB::table('laboratorios')
      ->where('id',$id)
      ->first();
      return view('laboratorios.edit',compact('laboratorio'));
    }

    public function update(Request $request, $id)
    {
      if(isset($request->link_archivo)){
        $path = Storage::disk('public')->putFileAs('laboratoriosimages',$request->file('link_archivo'),$request->link_archivo->getClientOriginalName());
        DB::table('laboratorios')
        ->where('id',$id)->update(
            [
              'laboratorio' => $request->laboratorio,
              'ruta' => "/".$path
            ]
        );
      }else{
        DB::table('laboratorios')
        ->where('id',$id)->update(
            [
              'laboratorio' => $request->laboratorio
            ]
        );
      }

      return redirect()->route('laboratorios.index')->with('info','Se actualizó correctamente');
    }
    public function store(Request $request)
    {
      $path = Storage::disk('public')->putFileAs('laboratoriosimages',$request->file('link_archivo'),$request->link_archivo->getClientOriginalName());
      DB::table('laboratorios')
      ->insert(
          [
            'laboratorio' => $request->laboratorio,
            'ruta' => "/".$path
          ]
      );
      return redirect()->route('laboratorios.index')->with('info','Se guardó correctamente');
    }
    public function destroy(Request $request, $id)
    {
      DB::table('laboratorios')
      ->where('id',$id)->update(
          [
            'estado' => 2
          ]
      );
      return redirect()->route('laboratorios.index')->with('info','Se eliminó correctamente');
    }
    public function laboratorioAs(Request $request)
    {
      $pal=strtoupper($request->term);

       $labs=DB::table('laboratorios')
       ->where('estado',1)
       ->where('laboratorio','LIKE', "%$pal%")
       ->limit(7)
       ->get();


      foreach ($labs as $l) {
         $rLabs[]=[
           'nombre'=>$l->laboratorio,
           'label'=>$l->laboratorio,
           'id'=>$l->id,
         ];

      }
      return Response($rLabs);

    }
}
