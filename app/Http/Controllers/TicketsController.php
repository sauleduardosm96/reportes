<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TicketsController extends Controller
{
    public function index()
    {
      return view('tickets.crearticket');
    }

    public function store(Request $request)
    {
      return view('tickets.creadoticket',compact('request'));
    }

    public function ticketsAs(Request $request)
    {
      $pal=strtoupper($request->term);

       $clientes=DB::table('clientes')
       ->where('estado',1)
       ->where('paga',1)
       ->where('nombre','LIKE', "%$pal%")
       ->limit(7)
       ->get();


      foreach ($clientes as $c) {
         $rCliente[]=[
           'nombre'=>$c->nombre,
           'label'=>$c->id." ".$c->nombre,
           'dato3'=>$c->nombre,
           'dato4'=>$c->nombre,
           'clave'=>$c->id,
           'direccion'=>$c->direccion.", COLONIA: ".$c->colonia.", POBLACIÓN: ".$c->poblacion,
           'telefono'=>$c->telefono,
           'id'=>$c->id,
         ];

      }
      return Response($rCliente);

    }
    public function ticketAs(Request $request)
    {
      $pal=strtoupper($request->term);

       $clientes=DB::table('clientes')
       ->where('estado',1)
       ->where('nombre','LIKE', "%$pal%")
       ->limit(7)
       ->get();


      foreach ($clientes as $c) {
         $rCliente[]=[
           'nombre'=>$c->nombre,
           'label'=>$c->id." ".$c->nombre,
           'dato3'=>$c->nombre,
           'dato4'=>$c->nombre,
           'clave'=>$c->id,
           'direccion'=>$c->direccion.", COLONIA: ".$c->colonia.", POBLACIÓN: ".$c->poblacion,
           'telefono'=>$c->telefono,
           'id'=>$c->id,
         ];

      }
      return Response($rCliente);

    }
    public function ticketPAs(Request $request)
    {
      $pal=strtoupper($request->term);

       $proveedores=DB::table('proveedores')
       ->where('estado',1)
       ->where('nombre','LIKE', "%$pal%")
       ->limit(7)
       ->get();


      foreach ($proveedores as $c) {
         $rProveedor[]=[
           'nombre'=>$c->nombre,
           'label'=>$c->id." ".$c->nombre,
           'dato3'=>$c->nombre,
           'dato4'=>$c->nombre,
           'clave'=>$c->id,
           'direccion'=>$c->direccion.", COLONIA: ".$c->colonia.", POBLACIÓN: ".$c->poblacion,
           'telefono'=>$c->telefono,
           'id'=>$c->id,
         ];

      }
      return Response($rProveedor);

    }
}
