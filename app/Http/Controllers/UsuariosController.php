<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{
    public function index(Request $request)
    {

      $busqueda = $request->get('busquedaId');
      $usuarios = DB::table('users')
      ->where('name','LIKE', "%$busqueda%")
      ->where('estado',1)
      ->paginate(20);
      return view('usuarios.index',compact('usuarios'));

    }

    public function edit($id)
    {
      $usuario = DB::table('users')->find($id);
      return view('usuarios.edit',compact('usuario'));
    }

    public function cambiar()
    {
      $user = auth()->user();
      return view('usuarios.cambiar',compact('user'));
    }

    public function cambio(Request $request)
    {
      $user = auth()->user()->id;
      $usuario=User::find($user);
      $usuario->password=Hash::make($request->pass);
      $usuario->save();
      return redirect()->route('cambiar')->with('info','Se ha cambiado la contraseña correctamente');
    }

    public function create()
    {
      return view('usuarios.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'email' => 'unique:users',
        ]);

        $usuario = new User;
        $usuario->name=$request->name;
        $usuario->email=$request->email;
        $usuario->password= Hash::make($request->pass);
        $usuario->save();
        return redirect()->route('usuarios.index')->with('info','Se ha guardado correctamente');


    }

    public function update(Request $request,$id)
    {

      $existe=User::where('email','=',$request->email)->where('id','!=',$id)->first();
      $validatedData = $request->validate([
        'email' => 'unique:users,email,' . $id,
      ]);
      $usuario=User::find($id);
      if($request->pass!=null){
        $usuario->password=Hash::make($request->pass);
      }
      $usuario->name=$request->name;
      $usuario->email=$request->email;
      $usuario->save();
      return redirect()->route('usuarios.index')->with('info','Se ha actualizado correctamente');

    }

    public function destroy($id)
    {
      $usuario=User::find($id);
      $usuario->estado=2;
      $usuario->save();
      return redirect()->route('usuarios.index')->with('info','Se ha eliminado correctamente');
    }

}
