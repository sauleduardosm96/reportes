<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class MedicamentosController extends Controller
{
    public function index(Request $request)
    {
      $busqueda = $request->get('busquedaId');
      $medicamentos=DB::table('catalogo')
      ->selectRaw('*,catalogo.id as idcat')
      ->where('catalogo.estado',1)
      ->where(function ($query) use ($busqueda) {
          $query->where('nombre','LIKE', "%$busqueda%")
          ->orWhere('catalogo.descripcion','LIKE', "%$busqueda%")
          ->orWhere('catalogo.codigo','LIKE', "%$busqueda%")
          ->orWhere('catalogo.sustancia','LIKE', "%$busqueda%")
          ->orWhere('catalogo.indice','LIKE', "%$busqueda%")
          ->orWhere('catalogo.oferta','LIKE', "%$busqueda%")
          ->orWhere('laboratorios.laboratorio','LIKE', "%$busqueda%");

      })

      ->join('laboratorios','catalogo.id_laboratorio','=','laboratorios.id')
      ->paginate(10);
      return view('medicamentos.index',compact('medicamentos'));
    }

    public function create()
    {
      return view('medicamentos.create');
    }

    public function edit($id)
    {
      $medicamento=DB::table('catalogo')
      ->selectRaw('*, catalogo.id as idcat')
      ->join('laboratorios','catalogo.id_laboratorio','=','laboratorios.id')
      ->where('catalogo.id',$id)
      ->first();
      return view('medicamentos.edit',compact('medicamento'));
    }

    public function update(Request $request, $id)
    {
      if(isset($request->link_archivo)){
        $path = Storage::disk('public')->putFileAs('laboratoriosimages',$request->file('link_archivo'),$request->link_archivo->getClientOriginalName());
        DB::table('catalogo')
        ->where('id',$id)->update(
            [
              'nombre' => $request->medicamento,
              'id_laboratorio' => $request->laboratorio_id,
              'descripcion' => $request->descripcion,
              'codigo' => $request->codigo,
              'sustancia' => $request->sustancia,
              'indice' => $request->indice,
              'pmp' => $request->pmp,
              'precio1' => $request->precio1,
              'precio2' => $request->precio2,
              'iva' => $request->iva,
              'oferta' => $request->oferta,
              'rutai' => "/".$path
            ]
        );
      }else{
        DB::table('catalogo')
        ->where('id',$id)->update(
            [
              'nombre' => $request->medicamento,
              'id_laboratorio' => $request->laboratorio_id,
              'descripcion' => $request->descripcion,
              'codigo' => $request->codigo,
              'sustancia' => $request->sustancia,
              'indice' => $request->indice,
              'pmp' => $request->pmp,
              'precio1' => $request->precio1,
              'precio2' => $request->precio2,
              'iva' => $request->iva,
              'oferta' => $request->oferta
            ]
        );
      }
      return redirect()->route('medicamentos.index')->with('info','Se actualizó correctamente');
    }
    public function store(Request $request)
    {
      $path = Storage::disk('public')->putFileAs('laboratoriosimages',$request->file('link_archivo'),$request->link_archivo->getClientOriginalName());

      DB::table('catalogo')
      ->insert(
          [
            'nombre' => $request->medicamento,
            'id_laboratorio' => $request->laboratorio_id,
            'descripcion' => $request->descripcion,
            'codigo' => $request->codigo,
            'sustancia' => $request->sustancia,
            'indice' => $request->indice,
            'pmp' => $request->pmp,
            'precio1' => $request->precio1,
            'precio2' => $request->precio2,
            'iva' => $request->iva,
            'oferta' => $request->oferta,
            'rutai' => "/".$path
          ]
      );
      return redirect()->route('medicamentos.index')->with('info','Se guardó correctamente');
    }
    public function destroy(Request $request, $id)
    {
      DB::table('catalogo')
      ->where('id',$id)->update(
          [
            'estado' => 2
          ]
      );
      return redirect()->route('medicamentos.index')->with('info','Se eliminó correctamente');
    }
    public function catalogos(Request $request)
    {

      $medicamentos=DB::table('catalogo')
      ->selectRaw('*,catalogo.id as idcat, catalogo.estado as est')
      ->join('laboratorios','catalogo.id_laboratorio','=','laboratorios.id')
      ->paginate(9);
      return view('medicamentos.catalogo',compact('medicamentos'));
    }
}
