<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AbonosController extends Controller
{

    public function edit($id)
    {
      $abonos=DB::table('abonos')
      ->where('id_compra',$id)
      ->where('estado',1)
      ->orderBy('fecha','desc')
      ->get();
      $compra=DB::table('compras')
      ->where('compras.id',$id)
      ->selectRaw('compras.*,proveedores.*, compras.id as idre')
      ->join('proveedores','compras.id_proveedor','=','proveedores.id')
      ->first();
      return view('abonos.edit',compact('abonos','compra'));
    }

    public function update(Request $request, $id)
    {
      app('App\Http\Controllers\proveedoresController')->comprueba();
      $abonos=DB::table('abonos')
      ->where('estado',1)
      ->where('id_compra',$id)
      ->orderBy('fecha','asc')
      ->get();

      $cont=0;
      foreach($abonos as $p){
        $cont=$cont+$p->cantidad;
      }
      $compra=DB::table('compras')
      ->where('id',$id)
      ->first();
      $restan=$compra->importe-$cont;
      if($request->cantidad>$restan){
        return redirect()->route('abonos.edit',$id)->with('info','la cantidad excede el restante');
      }else{
        DB::table('abonos')
        ->insert(
            [
              'cantidad' => $request->cantidad,
              'descripcion'=> $request->descripcion,
              'fecha' => $request->fecha,
              'id_compra' => $id,
            ]
        );
        return redirect()->route('abonos.edit',$id)->with('info','Se guardó correctamente');
      }

    }
    public function store(Request $request, $id)
    {

    }
    public function destroy(Request $request, $id)
    {
      DB::table('abonos')
      ->where('id',$id)->update(
          [
            'estado' => 2
          ]
      );
    return redirect()->route('abonos.edit',$request->val)->with('info','Se eliminó correctamente');
    }

}
