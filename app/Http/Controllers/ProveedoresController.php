<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProveedoresController extends Controller
{
    public function index(Request $request)
    {
      $this->comprueba();
      $busqueda = $request->get('busquedaId');
      $proveedores=DB::table('proveedores')
      ->where('nombre','LIKE', "%$busqueda%")
      ->where('estado',1)
      ->paginate(10);
      return view('proveedores.index',compact('proveedores'));
    }

    public function create()
    {
      return view('proveedores.create');
    }

    public function edit($id)
    {
      $proveedor=DB::table('proveedores')
      ->where('id',$id)
      ->first();
      return view('proveedores.edit',compact('proveedor'));
    }

    public function update(Request $request, $id)
    {
      DB::table('proveedores')
      ->where('id',$id)->update(
          [
            'nombre' => $request->nombre,

            'direccion'=> $request->direccion,
            'negocio' => $request->negocio,
            'colonia' => $request->colonia,
            'poblacion' => $request->poblacion,
            'telefono' => $request->telefono,
            'correo' => $request->correo
          ]
      );
      return redirect()->route('proveedores.index')->with('info','Se actualizó correctamente');
    }
    public function store(Request $request)
    {
      DB::table('proveedores')
      ->insert(
          [
            'nombre' => $request->nombre,

            'direccion'=> $request->direccion,
            'negocio' => $request->negocio,
            'colonia' => $request->colonia,
            'poblacion' => $request->poblacion,
            'telefono' => $request->telefono,
            'correo' => $request->correo
          ]
      );
      return redirect()->route('proveedores.index')->with('info','Se guardó correctamente');
    }
    public function destroy(Request $request, $id)
    {
      DB::table('proveedores')
      ->where('id',$id)->update(
          [
            'estado' => 2
          ]
      );
      return redirect()->route('proveedores.index')->with('info','Se eliminó correctamente');
    }

    public function comprueba(){
      $compras=DB::table('compras')
      ->selectRaw('compras.*,proveedores.*, compras.id as idre,proveedores.id as idpro')
      ->where('compras.estado',1)
      ->join('proveedores','compras.id_proveedor','=','proveedores.id')
      ->get();
      $abonos=DB::table('abonos')
      ->where('estado',1)
      ->orderBy('fecha','asc')
      ->get();
      $deudores = [];
      foreach($compras as $r){
        $cont=0;
        $ultimop="";
        foreach($abonos as $p){
          if($p->id_compra==$r->idre){
            $cont=$cont+$p->cantidad;
            $ultimop=$p->fecha;
          }
        }
        $ultimoAbono = new \DateTime($ultimop);
        $hoy = new \DateTime("NOW");
        $hoy = new \DateTime($hoy->format("Y-m-d"));
        $fechaEstipulada = new \DateTime($r->fechafin);
        //$diff = $utlimoPago->diff($fechaEstipulada);
        if($cont>=$r->importe){
          //ya está pagado
          //si el último abono es antes de cumplir la fecha estipulada pasa a Completado
          if($ultimoAbono<=$fechaEstipulada){
            DB::table('compras')
            ->where('id',$r->idre)->update(
                [
                  'abono' => 1
                ]
            );
            //si no, pasa a completado a destiempo
          }else{
            DB::table('compras')
            ->where('id',$r->idre)->update(
                [
                  'abono' => 2
                ]
            );
          }
        }else{
          //no está pagado
          //si aún quedan días para efectuar su abono, no hacer nada
          if($hoy<=$fechaEstipulada){
            DB::table('compras')
            ->where('id',$r->idre)->update(
                [
                  'abono' => 3
                ]
            );
            //si no quedan días, el proveedor pasa a deudor en el array
          }else{
            array_push($deudores,$r->id_proveedor);
            DB::table('compras')
            ->where('id',$r->idre)->update(
                [
                  'abono' => 4
                ]
            );
          }
        }
      }
      /*DB::table('proveedores')
      ->update(
          [
            'paga' => 1
          ]
      );

      //ejecutamos a los proveedores deudores
      foreach ($deudores as $d) {
        DB::table('proveedores')
        ->where('id',$d)->update(
            [
              'paga' => 2
            ]
        );
      }*/
    }
}
