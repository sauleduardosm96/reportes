<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RutasController extends Controller
{
    public function index(Request $request)
    {
      $busqueda = $request->get('busquedaId');
      $rutas=DB::table('rutas')
      ->where('ruta','LIKE', "%$busqueda%")
      ->where('estado',1)
      ->paginate(10);
      return view('rutas.index',compact('rutas'));
    }

    public function create()
    {
      return view('rutas.create');
    }

    public function edit($id)
    {
      $ruta=DB::table('rutas')
      ->where('id',$id)
      ->first();
      return view('rutas.edit',compact('ruta'));
    }

    public function update(Request $request, $id)
    {
        DB::table('rutas')
        ->where('id',$id)->update(
            [
              'ruta' => $request->ruta
            ]
        );
      return redirect()->route('rutas.index')->with('info','Se actualizó correctamente');
    }
    public function store(Request $request)
    {
      DB::table('rutas')
      ->insert(
          [
            'ruta' => $request->ruta,
          ]
      );
      return redirect()->route('rutas.index')->with('info','Se guardó correctamente');
    }
    public function destroy(Request $request, $id)
    {
      DB::table('rutas')
      ->where('id',$id)->update(
          [
            'estado' => 2
          ]
      );
      return redirect()->route('rutas.index')->with('info','Se eliminó correctamente');
    }
    public function rutasAs(Request $request)
    {
      $pal=strtoupper($request->term);

       $rutas=DB::table('rutas')
       ->where('estado',1)
       ->where('ruta','LIKE', "%$pal%")
       ->limit(7)
       ->get();


      foreach ($rutas as $l) {
         $rRutas[]=[
           'nombre'=>$l->ruta,
           'label'=>$l->ruta,
           'id'=>$l->id,
         ];

      }
      return Response($rRutas);

    }
}
