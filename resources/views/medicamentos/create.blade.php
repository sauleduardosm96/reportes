@extends('layouts.main')

@section('seccion')
  Medicamentos / Crear
@endsection

@section('titulo')
  Nuevo Medicamento
@endsection

@section('descripcion')
  Agregar datos de medicamento
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')

  <form action="{{route('medicamentos.store')}}" enctype="multipart/form-data"  method="POST">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
      <label for="exampleForm2">Medicamento</label>
      <input type="text" required name="medicamento" id="medicamento" placeholder="Medicamento"class="form-control">
    </div>
    <label for="exampleForm2">Laboratorio</label>
    <div class="input-group mb-2 mr-sm-2">
      <input type="text" id="laboratorio" required name="laboratorio" class="form-control" placeholder="Laboratorio">
      <input type="text" id="laboratorio_id" hidden name="laboratorio_id" class="form-control" placeholder="laboratorio_id">
      <div class="input-group-prepend">
       <a class="input-group-text btn btn-danger " onclick="limpiarCampo()">
        <i class="fa fa-trash-o"></i>
       </a>
      </div>
    </div>
    <div class="form-group">
      <label for="exampleForm2">Descripción</label>
      <input type="text" required name="descripcion" id="descripcion" placeholder="Descripcion"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Codigo</label>
      <input type="text" required name="codigo" id="codigo" placeholder="Codigo"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Sustancia</label>
      <input type="text" required name="sustancia" id="sustancia" placeholder="Sustancia"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Índice</label>
      <input type="text" required name="indice" id="indice" placeholder="Índice"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">PMP</label>
      <input type="number" step="0.01" required name="pmp" id="pmp" placeholder="PMP"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Precio 1</label>
      <input type="number" step="0.01" required name="precio1" id="precio1" placeholder="Precio 1"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Precio 2</label>
      <input type="number" step="0.01" required name="precio2" id="precio2" placeholder="Precio 2"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">IVA</label>
      <input type="number" step="0.01" required name="iva" id="iva" placeholder="IVA"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Oferta</label>
      <input type="text" required name="oferta" id="oferta" placeholder="Oferta"class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Imagen del medicamento</label>
      <input type="file" required name="link_archivo" id="link_archivo"  type="file" accept="image/*" class="form-control">
    </div>


    <!--
faltan campos
vista de catalogo
añadir laboratorio a asincrono
  -->



    <div class="d-flex">
      <div class="mr-auto p-2">
        <button type="submit" class="btn  btn-info text-white">Guardar</button>
        <a href="{{route('medicamentos.index')}}" class="btn  btn-secondary">Cancelar</a>
      </div>
    </div>
  </form>
  <script type="text/javascript">
  $(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});
  function limpiarCampo() {
    $("#laboratorio").val("");
    $("#laboratorio_id").val("");
    $("#laboratorio").attr('required', true);
    $("#laboratorio").attr('disabled', false);
  }
  const lab = {
       source: '/laboratorioAs',
       minLength: 2,
       autoFocus: true,
       select:function (e,ui) {
          $("#laboratorio").val(ui.item.laboratorio);
          $("#laboratorio_id").val(ui.item.id);
          $("#laboratorio").attr('disabled', true);
       }
  };

  $('#laboratorio').autocomplete(lab);
  </script>
@endsection
