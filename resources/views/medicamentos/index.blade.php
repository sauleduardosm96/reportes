@extends('layouts.main')

@section('seccion')
  Laboratorios
@endsection

@section('titulo')
  Gestor de medicamentos
@endsection

@section('boton')
  <a href="{{route('medicamentos.create')}}" class="btn  btn-info text-white"><i class="fa fa-plus"></i> Registrar nuevo medicamento</a>
@endsection

@section('descripcion')
  Panel de control para gestionar los medicamentos
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection


@section('contenido')
{{Form::open(['route'=>'medicamentos.index','method'=>'GET'])}}
  @include('layouts.busqueda')
{{Form::close()}}
  <table class="table  table-hover table-outline text-center">
    <thead class="thead-light">
      <tr>
        <th scope="col">Código</th>
        <th scope="col">Medicamento</th>
        <th scope="col">Laboratorio</th>
        <th scope="col">Descripción</th>
        <th scope="col">Sustancia Activa</th>
        <th scope="col">Índice Terapéutico</th>
        <th scope="col">P.M.P.</th>
        <th scope="col">Precio 1</th>
        <th scope="col">Precio 2</th>
        <th scope="col">IVA</th>
        <th scope="col">Oferta</th>
        <th scope="col">Imagen</th>
        <th scope="col">Editar</th>
      </tr>
    </thead>
    <tbody>
      @foreach($medicamentos as $c)
        <tr>
          <td>{{$c->codigo}}</td>
          <td>{{$c->nombre}}</td>
          <td>{{$c->laboratorio}}</td>
          <td>{{$c->descripcion}}</td>
          <td>{{$c->sustancia}}</td>
          <td>{{$c->indice}}</td>
          <td>{{$c->pmp}}</td>
          <td>{{$c->precio1}}</td>
          <td>{{$c->precio2}}</td>
          <td>{{$c->iva}}</td>
          <td>{{$c->oferta}}</td>
          <td><img height="150px" src="{{asset('storage'.$c->rutai)}}" alt=""></td></td>
          <td>
            <a class="btn  btn-info text-white" href="{{route('medicamentos.edit',$c->idcat)}}"><i class="icon-pencil "></i></a>
          </td>
        </tr>
      @endforeach

    </tbody>
  </table>
  {!!$medicamentos->appends(Request::only(['medicamento']))->render()!!}
  <script type="text/javascript">
    function cerrar() {
        $('#not').remove();
    }
  </script>

@endsection
