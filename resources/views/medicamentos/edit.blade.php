@extends('layouts.main')

@section('seccion')
  Medicamentos / Editar
@endsection

@section('titulo')
  Medicamento: {{$medicamento->nombre}}
@endsection

@section('descripcion')
  Editar datos de medicamento
@endsection



@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')

  <form action="{{route('medicamentos.update',$medicamento->idcat)}}"  enctype="multipart/form-data" method="POST">
    <input type="hidden" name="_method" value="PUT">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

      <div class="form-group">
        <label for="exampleForm2">Medicamento</label>
        <input type="text" required value="{{$medicamento->nombre}}" name="medicamento" id="medicamento" placeholder="Medicamento"class="form-control">
      </div>
      <label for="exampleForm2">Laboratorio</label>
      <div class="input-group mb-2 mr-sm-2">
        <input type="text" id="laboratorio" required disabled value="{{$medicamento->laboratorio}}"name="laboratorio" class="form-control" placeholder="Medicamento">
        <input type="text" id="laboratorio_id" hidden value="{{$medicamento->id_laboratorio}}"name="laboratorio_id" class="form-control" placeholder="medicamento_id">
        <div class="input-group-prepend">
         <a class="input-group-text btn btn-danger " onclick="limpiarCampo()">
          <i class="fa fa-trash-o"></i>
         </a>
        </div>
      </div>
      <div class="form-group">
        <label for="exampleForm2">Descripción</label>
        <input type="text" required name="descripcion" value="{{$medicamento->descripcion}}" id="descripcion" placeholder="Descripcion"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Codigo</label>
        <input type="text" required name="codigo" id="codigo" value="{{$medicamento->codigo}}" placeholder="Codigo"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Sustancia</label>
        <input type="text" required name="sustancia" id="sustancia" value="{{$medicamento->sustancia}}" placeholder="Sustancia"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Índice</label>
        <input type="text" required name="indice" id="indice" value="{{$medicamento->indice}}" placeholder="Índice"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">PMP</label>
        <input type="number" step="0.01" required name="pmp" id="pmp" value="{{$medicamento->pmp}}" placeholder="PMP"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Precio 1</label>
        <input type="number" step="0.01" required name="precio1" id="precio1" value="{{$medicamento->precio1}}" placeholder="Precio 1"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Precio 2</label>
        <input type="number" step="0.01" required name="precio2" id="precio2" value="{{$medicamento->precio2}}" placeholder="Precio 2"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">IVA</label>
        <input type="number" step="0.01" required name="iva" id="iva" value="{{$medicamento->iva}}" placeholder="IVA"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Oferta</label>
        <input type="text" required name="oferta" id="oferta" value="{{$medicamento->oferta}}" placeholder="Oferta"class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Imagen del medicamento</label>
        <input type="file" name="link_archivo" id="link_archivo"  type="file" accept="image/*" class="form-control">
      </div>
      <div class="d-flex">
        <div class="mr-auto p-2">
          <button type="submit" class="btn  btn-info text-white">Actualizar</button>
          <a href="{{route('medicamentos.index')}}" class="btn  btn-secondary">Cancelar</a>
        </div>
  </form>
  <div class="p-2"><button type="button" onclick="$('#formDes').submit();" class="btn  btn-danger">Eliminar</button></div>
</div>
{!! Form::model($medicamento,['route'=>['medicamentos.destroy', $medicamento->id], 'method'=>'DELETE', 'id'=>'formDes','onsubmit'=>'return confirm("¿Seguro que desea eliminarlo?")']) !!}
{!!Form::close()!!}
<script type="text/javascript">
$(document).on("wheel", "input[type=number]", function (e) {
  $(this).blur();
});
function limpiarCampo() {
  $("#laboratorio").val("");
  $("#laboratorio_id").val("");
  $("#laboratorio").attr('required', true);
  $("#laboratorio").attr('disabled', false);
}
const lab = {
     source: '/laboratorioAs',
     minLength: 2,
     autoFocus: true,
     select:function (e,ui) {
        $("#laboratorio").val(ui.item.laboratorio);
        $("#laboratorio_id").val(ui.item.id);
        $("#laboratorio").attr('disabled', true);
     }
};

$('#laboratorio').autocomplete(lab);
</script>
@endsection
