@extends('layouts.main')

  @section('seccion')
  <div class="oculto-impresion">
    Laboratorios
  </div>
  @endsection



@section('titulo')
  Catálogo de medicamentos
  <img src="{{asset('pan/img/brand/logo.png')}}" alt="" width="150px">
@endsection

@section('boton')

@endsection

@section('descripcion')
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection


@section('contenido')
<div class="container">
  <div class="row">
      @foreach($medicamentos as $c)
      @if($c->est==1)
        <div class="col text-justify">
          <div class="card" style="width: 18rem;">
            @if($c->rutai=="")
              <img class="card-img-top" src="{{asset('storage'.'/laboratoriosimages/defaultMedic.jpg')}}" height="110px" alt="Card image cap" style="margin-bottom:-10px">
            @else
              <img class="card-img-top" src="{{asset('storage'.$c->rutai)}}"  height="110px" alt="Card image cap" style="margin-bottom:-10px">
            @endif
            <div class="card-body">
              <p class="card-text"><strong>Código: </strong>{{$c->codigo}}</p>
              <h5 class="card-title">Nombre: {{$c->nombre}}</h5>
              <h6><strong>Laboratorio: </strong>{{$c->laboratorio}}
                @if($c->ruta=="")
                  <img class="" src="{{asset('storage'.'/laboratoriosimages/default.jpg')}}" height="15px"  alt="Card image cap">
                @else
                  <img class="" src="{{asset('storage'.$c->ruta)}}"  height="15px"  alt="Card image cap">
                @endif
              </h6>
              <p class="card-text"><strong>Descripcion: </strong>{{$c->descripcion}}</p>
              <p class="card-text"><strong>Sustancia: </strong>{{$c->sustancia}}</p>
              <p class="card-text"><strong>Indice: </strong>{{$c->indice}}</p>
              <div class="row">
                <div class="col">
                  <strong>P.M.P: </strong>${{number_format($c->pmp,2)}}
                </div>
                <div class="col">
                  <strong>Precio 1: </strong>${{number_format($c->precio1,2)}}
                </div>
                <div class="col">
                  <strong>Precio 2: </strong>${{number_format($c->precio2,2)}}
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <strong>I.V.A.: </strong>${{number_format($c->iva,2)}}
                </div>
                <div class="col">
                  <strong>Oferta: </strong>{{$c->oferta}}
                </div>
              </div>
            </div>
          </div>
          <!--<tr>
            <td>{{$c->iva}}</td>
            <td>{{$c->oferta}}</td>
            <td><img height="150px" src="{{asset('storage'.$c->rutai)}}" alt=""></td></td>
            <td>
              <a class="btn  btn-info text-white" href="{{route('medicamentos.edit',$c->idcat)}}"><i class="icon-pencil "></i></a>
            </td>
          </tr>-->
        </div>
      @else
      <div class="col text-justify">
      </div>
      @endif
      @endforeach
  </div>
</div>
<div class="oculto-impresion">

  {!!$medicamentos->appends(Request::only(['medicamento']))->render()!!}
</div>
  <style>
  @media print{

.oculto-impresion, .oculto-impresion *{

  display: none !important;

}
.card {
  border: 0px !important;
}

}
  </style>
  <script type="text/javascript">
    function cerrar() {
        $('#not').remove();
    }
  </script>

@endsection
