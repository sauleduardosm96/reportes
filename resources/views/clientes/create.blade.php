@extends('layouts.main')

@section('seccion')
  Clientes / Crear
@endsection

@section('titulo')
  Nuevo Cliente
@endsection

@section('descripcion')
  Agregar datos de cliente
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')

  <form action="{{route('clientes.store')}}"  method="POST">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
      <label for="exampleForm2">Nombre</label>
      <input type="text" name="nombre" id="nombre" class="form-control">
    </div>

    <div class="form-group">
      <label for="exampleForm2">Negocio</label>
      <input type="text" name="negocio" id="negocio" class="form-control">
    </div>

    <div class="form-group">
      <label for="exampleForm2">Dirección</label>
      <input type="text" name="direccion" id="direccion" class="form-control">
    </div>

    <div class="form-group">
      <label for="exampleForm2">Colonia</label>
      <input type="text" name="colonia" id="colonia" class="form-control">
    </div>

    <div class="form-group">
      <label for="exampleForm2">Población</label>
      <input type="text" name="poblacion" id="poblacion" class="form-control">
    </div>

    <div class="form-group">
      <label for="exampleForm2">Teléfono</label>
      <input type="text" name="telefono" id="telefono" class="form-control">
    </div>

    <div class="form-group">
      <label for="exampleForm2">Correo</label>
      <input type="text" name="correo" id="correo" class="form-control">
    </div>

    <div class="form-group">
      <label for="exampleForm2">Ruta</label>
      <input type="text" id="ruta" name="ruta" class="form-control" placeholder="Ruta">
      <input type="hidden" id="ruta_id" name="ruta_id" class="form-control" placeholder="Nombre">
    </div>

    <div class="d-flex">
      <div class="mr-auto p-2">
        <button type="submit" class="btn  btn-info text-white">Guardar</button>
        <a href="{{route('clientes.index')}}" class="btn  btn-secondary">Cancelar</a>
      </div>
    </div>
  </form>
  <script type="text/javascript">
  function limpiarCampo() {
    $("#ruta").val("");
    $("#ruta_id").val("");
    $("#ruta").attr('required', true);
    $("#ruta").attr('disabled', false);
  }
  const ruta = {
       source: '/rutasAs',
       minLength: 2,
       autoFocus: true,
       select:function (e,ui) {
          $("#ruta").val(ui.item.ruta);
          $("#ruta_id").val(ui.item.id);
          $("#ruta").attr('disabled', true);
       }
  };

  $('#ruta').autocomplete(ruta);
  </script>
@endsection
