
<div class="form-group">
  <label for="name">{{ __('Nombre') }}</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name','required','placeholder'=>'Nombre']) !!}
  @if ($errors->has('name'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
  @endif
</div>
<div class="form-group">
  <label for="email" >{{ __('Cuenta') }}</label>
  {!! Form::email('email', null, ['class'=>'form-control', 'id'=>'email','required','placeholder'=>'Cuenta']) !!}
  @if ($errors->has('email'))
    <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('email') }}</strong>
    </span>
  @endif
</div>

<div class="form-group">
  <label for="pass">Contraseña</label>
  <input type="password" name="pass" class="form-control" id="pass" onkeyup="requerido();" required minlength=8 placeholder="Contraseña">
</div>
<div class="form-group">
  <label for="passcon">Confirmar Contraseña</label>
  <input type="password" name="passcon" class="form-control" id="passcon" onkeyup="requerido();" required minlength=8 placeholder="Contraseña">
</div>
<div class="alert alert-danger" id="alertCon" style="display:none" role="alert">
  Las contraseñas no coinciden
</div>
<script type="text/javascript">
  $( document ).ready(function() {
    $("select option:contains('FORÁNEAS')").attr("disabled","disabled");
  });

  $( document ).ready(function() {
    if($('#formEdit').length){
      $('#pass').removeAttr("required");
      $('#passcon').removeAttr("required");
    }
  });

  function validar() {
    if($('#pass').val()!=$('#passcon').val()){
      console.log("no son iguales");
      $('#alertCon').show();
      event.preventDefault();
    }else{
      console.log("si son iguales");
      confirmar();
    }
  }

  function requerido(){
    if($('#formEdit').length){
      if($('#pass').val()!="" || $('#passcon').val()!=""){
        console.log("requerido");
        $('#pass').prop("required", true);
        $('#passcon').prop("required", true);
      }else{
        console.log("No requerido");
        $('#pass').removeAttr("required");
        $('#passcon').removeAttr("required");
      }
    }
  }

</script>
