@extends('layouts.main')

@section('seccion')
  Usuarios / Editar
@endsection

@section('titulo')
  Usuario: {{$usuario->name}}
@endsection

@section('descripcion')
  Editar datos de usuario
@endsection



@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')
  {!! Form::model($usuario,['route'=>['usuarios.update', $usuario->id], 'method'=>'PUT', 'id'=>'formEdit','onsubmit'=>'validar()']) !!}
    @include('usuarios.form')
    <div class="d-flex">
      <div class="mr-auto p-2">
        <button type="submit" class="btn  btn-info text-white">Actualizar</button>
        <a href="{{route('usuarios.index')}}" class="btn  btn-secondary">Cancelar</a>
      </div>
      {!!Form::close()!!}
      <div class="p-2"><button type="button" onclick="$('#formDes').submit();" class="btn  btn-danger">Eliminar</button></div>
    </div>
    {!! Form::model($usuario,['route'=>['usuarios.destroy', $usuario->id], 'method'=>'DELETE', 'id'=>'formDes','onsubmit'=>'return confirm("¿Seguro que desea eliminarlo?")']) !!}
    {!!Form::close()!!}
@endsection

<script type="text/javascript">
  function confirmar() {
    if (confirm("¿Seguro que desea actualizarlo?") != true) {
      event.preventDefault();
    }
  }
</script>
