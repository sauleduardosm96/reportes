@extends('layouts.main')

@section('seccion')
  Usuarios / Cambiar Contraseña
@endsection

@section('titulo')
  Usuario: {{ $user->name }}
@endsection

@section('descripcion')
  Cambiar contraseña
@endsection

@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')
  {!!Form::open(['route'=> 'cambio','files'=>'false','id'=>'formCreate','onsubmit'=>'validar()'])!!}
  <div class="form-group">
    <label for="pass">Contraseña</label>
    <input type="password" name="pass" class="form-control" id="pass" required minlength=8 placeholder="Contraseña">
  </div>
  <div class="form-group">
    <label for="passcon">Confirmar Contraseña</label>
    <input type="password" name="passcon" class="form-control" id="passcon"  required minlength=8 placeholder="Contraseña">
  </div>
  <div class="alert alert-danger" id="alertCon" style="display:none" role="alert">
    Las contraseñas no coinciden
  </div>
    <div class="d-flex">
      <div class="mr-auto p-2">
        <button type="submit" class="btn  btn-info text-white">Guardar</button>
        <a href="{{route('usuarios.index')}}" class="btn  btn-secondary">Cancelar</a>
      </div>
    </div>
  </form>

@endsection

<script type="text/javascript">
  function confirmar() {
    if (confirm("¿Seguro que desea cambiar la contraseña?") != true) {
      event.preventDefault();
    }
  }

  function validar() {
    if($('#pass').val()!=$('#passcon').val()){
      console.log("no son iguales");
      $('#alertCon').show();
      event.preventDefault();
    }else{
      console.log("si son iguales");
      confirmar();
    }
  }
</script>
