<script>

const proveedor = {
     source: '/ticketPAs',
     minLength: 2,
     autoFocus: true,
     select:function (e,ui) {

        $("#nombreAlt").val(ui.item.nombre);
        $("#direccion").val(ui.item.direccion);
        $("#telefono").val(ui.item.telefono);
        $("#id").val(ui.item.id);
        $("#nombre").attr('disabled', true);
        $("#inicial").attr('required', true);
        $("#final").attr('required', true);
     }
};

$('#nombre').autocomplete(proveedor);


function limpiarCampo() {
  $("#nombreAlt").val("");
  $("#nombre").val("");
  $("#direccion").val("");
  $("#clave").val("");
  $("#telefono").val("");
  $("#nombre").attr('disabled', false);
}

  function calcularFinal() {
    console.log("aquí");
    var importeTotal = 0;
    for (var i = 1; i < 25; i++) {
      var temp;
      if($("#importe"+i).val()==""){
        temp=0;
        console.log("aqui vale " + temp);
      }else{
        temp=parseFloat($("#importe"+i).val());
        console.log("aqui vale " + temp);
      }
      importeTotal=parseFloat(importeTotal)+parseFloat(temp);
      console.log(importeTotal);
    }
    $("#importeTotal").val(parseFloat(importeTotal).toFixed(2));
  }
  $(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});

function calcular(id) {
  var itemp=parseFloat($("#precio"+id).val()).toFixed(2)*parseFloat($("#cantidad"+id).val()).toFixed(2);

  $("#importe"+id).val(itemp.toFixed(2));
  calcularFinal();
}

$(document).on("keypress", 'form', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        return false;
    }
});
</script>
