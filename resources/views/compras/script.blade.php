<script>
  const descripcion{{$i}} = {
       source: '/descripcionAs',
       minLength: 2,
       autoFocus: true,
       select:function (e,ui) {
          $("#descripcion{{$i}}").val(ui.item.descripcion);
       }
  };

  $('#descripcion{{$i}}').autocomplete(descripcion{{$i}});
</script>
