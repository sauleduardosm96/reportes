@extends('layouts.main')

@section('seccion')
  Compras
@endsection

@section('titulo')
  Gestor de compras
@endsection

@section('boton')
  <a href="{{route('compras.create')}}" class="btn  btn-info text-white"><i class="fa fa-plus"></i> Registrar nueva compra</a>
@endsection

@section('descripcion')
  Panel de control para gestionar las compras
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection


@section('contenido')
{{Form::open(['route'=>'compras.index','method'=>'GET'])}}
  @include('layouts.busqueda')
{{Form::close()}}
  <table class="table table-hover table-outline text-center">
    <thead class="thead-light">
          <tr>
            <th scope="col">Número de remisión</th>
            <th scope="col">Proveedor</th>
            <th scope="col">Fecha Inicio</th>
            <th scope="col">Fecha Vencimiento</th>
            <th scope="col">Importe</th>
            <th scope="col">Pagado</th>
            <th scope="col">Pendiente</th>
            <th scope="col">Días</th>
            <!--<th scope="col">Ver</th>-->
            <th scope="col">Editar</th>
            <th scope="col">Añadir abono</th>
          </tr>
        </thead>
        <tbody>
          @foreach($compras as $r)

              @php
              $fecha1 = new DateTime("NOW");
              $fecha1 = new \DateTime($fecha1->format("Y-m-d"));

              $fecha2 = new DateTime($r->fechafin);
              $diff = $fecha1->diff($fecha2);
              $cont=0;
              $palabra="";
              @endphp

              @if($r->abono==1)
                @php $palabra="Completado."; @endphp
                <tr class="table-success">
              @elseif($r->abono==2)
                @php $palabra="Completado a destiempo."; @endphp
                <tr class="table-warning">
              @elseif($r->abono==3)
                @if($diff->days!=0)
                  @php $palabra="Quedan: ".$diff->days." días."; @endphp
                @else
                  @php $palabra="Vence hoy"; @endphp
                @endif
                <tr>
              @elseif($r->abono==4)
                @php $palabra="Venció hace: ".$diff->days." días."; @endphp
                <tr class="table-danger">
              @endif
              <th scope="row">{{$r->idre}}</th>
              <td>{{$r->nombre}}</td>
              <td>{{$r->fecha}}</td>
              <td>{{$r->fechafin}}</td>
              <td>${{number_format($r->importe,2)}}</td>
              @foreach($abonos as $p)
                @if($p->id_compra==$r->idre)
                  @php
                    $cont=$cont+$p->cantidad;
                  @endphp
                @endif
              @endforeach
              <td>${{number_format($cont,2)}}</td>
              <td>${{number_format($r->importe-$cont,2)}}</td>
              <td>
                {{$palabra}}
              </td>
              <!--<td>
                <a class="btn  btn-info text-white" target="_blank" href="{{route('compras.show',$r->idre)}}"><i class="fa fa-file-pdf-o "></i></a>
              </td>-->
              <td>
                <a class="btn  btn-info text-white" href="{{route('compras.edit',$r->idre)}}"><i class="icon-pencil "></i></a>
              </td>
              <td>
                <a class="btn  btn-info text-white" href="{{route('abonos.edit',$r->idre)}}"><i class="fa fa-usd "></i></a>
              </td>
            </tr>
          @endforeach

        </tbody>
      </table>
      {!!$compras->appends(Request::only(['nombre']))->render()!!}
      @endsection
