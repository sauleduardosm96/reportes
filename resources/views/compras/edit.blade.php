@extends('layouts.main')

@section('seccion')
  Compras / Editar
@endsection

@section('titulo')
  Remisión Número: {{$compra->idre}}
@endsection

@section('descripcion')
  Editar datos de remisión
@endsection



@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')
    <div class="container text-center">
      <form action="{{route('compras.update',$compra->idre)}}" method="POST">
         <input type="hidden" name="_method" value="PUT">
         <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="row">
                  <div class="col-12">
                    <div class="text-center border border-light p-5" >
                        Proveedor
                        <div class="input-group mb-2 mr-sm-2">
                          <input type="text" disabled value="{{$compra->nombre}}" id="nombre" required name="nombre" class="form-control" style="text-transform: uppercase" placeholder="NOMBRE">
                          <input type="hidden" value="{{$compra->nombre}}" id="nombreAlt" name="nombreAlt" class="form-control" placeholder="Nombre">
                          <div class="input-group-prepend">
                           <a class="input-group-text btn  btn-danger" onclick="limpiarCampo()">
                            <i class="fa fa-trash-o"></i>
                           </a>
                          </div>
                        </div>
                        <input type="text" value="{{$compra->id_proveedor}}" hidden id="id" name="id"  class="form-control mb-2" >
                        <input type="text" value="{{$compra->direccion}}" id="direccion" name="direccion" readonly class="form-control mb-2" placeholder="DIRECCIÓN">
                        <input type="text" value="{{$compra->telefono}}" id="telefono" name="telefono" readonly class="form-control mb-2" placeholder="TELÉFONO">
                        <p>Fecha inicial</p>
                        <input type="date" value="{{$compra->fecha}}" id="fecha" required name="fecha" class="form-control mb-2" placeholder="FECHA">
                        <p>Fecha final</p>
                        <input type="date" value="{{$compra->fechafin}}" id="fechafin" required name="fechafin" class="form-control mb-2" placeholder="FECHA">

                    </div>
                  </div>
            </div>
            <div class="row">
                  <div class="col-12">
                    <div class="text-center border border-light p-5" >
                      <div class="row">
                        <div class="col">
                          Cantidad
                        </div>
                        <div class="col">
                          Descripción
                        </div>
                        <div class="col">
                          Precio Unitario
                        </div>
                        <div class="col">
                          IVA (%)
                        </div>
                        <div class="col">
                          Importe
                        </div>
                        <br><br>
                      </div>
                      @for($i = 1; $i < 25; $i++)
                      <div class="row">
                        <div class="col">
                          <input type="hidden" name="id{{$i}}" value="{{$articulos[$i-1]->id}}">
                          <input type="number" step="0.01"value="{{$articulos[$i-1]->cantidad}}" name="cantidad{{$i}}" id="cantidad{{$i}}" onkeyup="calcular({{$i}})" class="form-control" placeholder="Cantidad">
                        </div>
                        <div class="col">
                          <textarea name="descripcion{{$i}}" id="descripcion{{$i}}" class="form-control" placeholder="Descripción">{{$articulos[$i-1]->descripcion}}</textarea>
                        </div>
                        <div class="col">
                          <input type="number" step="0.01"value="{{$articulos[$i-1]->precio}}" name="precio{{$i}}" id="precio{{$i}}" onkeyup="calcular({{$i}})" class="form-control" placeholder="Precio Unitario">
                        </div>
                        <div class="col">
                          <input type="number"step="0.01" value="{{$articulos[$i-1]->iva}}" name="iva{{$i}}" id="iva{{$i}}" class="form-control" placeholder="IVA">
                        </div>
                        <div class="col">
                          <input type="number"step="0.01" value="{{$articulos[$i-1]->importe}}" name="importe{{$i}}"  id="importe{{$i}}" class="form-control importes" placeholder="Importe">
                        </div>
                        <br><br><br><br>
                      </div>
                      @include('compras.script')
                      @endfor
                    </div>
                  </div>
            </div>
            <div class="row">
                  <div class="col-12">
                    <div class="text-center border border-light p-5" >
                      <div class="row">
                        <div class="col-sm">
                          <input type="number" class="form-control" placeholder="Cantidad" hidden>
                        </div>
                        <div class="col-sm">
                          <input type="number" class="form-control" placeholder="Cantidad" hidden>
                        </div>
                        <div class="col-sm">
                          <input type="number" class="form-control" placeholder="Cantidad" hidden>
                        </div>
                        <div class="col-sm">
                          <h3>Importe total:</h3>
                        </div>
                        <div class="col-sm">
                          <input type="number" step="0.01"value="{{$compra->importe}}" id="importeTotal" name="importeTotal" class="form-control" placeholder="Importe Total" >
                        </div>
                        <br><br>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="d-flex">
              <div class="mr-auto p-2">
                <button type="submit" class="btn  btn-info text-white">Actualizar</button>
                <a href="{{route('compras.index')}}" class="btn  btn-secondary">Cancelar</a>
              </div>
              <div class="p-2"><button type="button" onclick="$('#formDes').submit();" class="btn  btn-danger">Eliminar</button></div>
            </div>
      </form>
    </div>
    {!! Form::model($compra,['route'=>['compras.destroy', $compra->idre], 'method'=>'DELETE', 'id'=>'formDes','onsubmit'=>'return confirm("¿Seguro que desea eliminarlo?")']) !!}
    {!!Form::close()!!}

  <script type="text/javascript">
    function cerrar() {
        $('#not').remove();
    }
  </script>
  @include('compras.scripjs')
@endsection
