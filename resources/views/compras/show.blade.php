<?php
include(app_path().'\..\public\Fpdf181\fpdf.php');
class PDF extends FPDF
{
	function Header(){
			$this->Image('../public/img/fondo.png',-1,-2,216.5);
      $this->SetFont('Arial','',14);
			$this->SetTextColor(227,28,36);
			$this->text(182,17.1,utf8_decode($this->r->idre));
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','',10);
      $this->text(172.5,35,utf8_decode($this->fechao));
			$this->SetFont('Arial','',9);
      $this->text(37,51.5,utf8_decode($this->r->id));
      $this->text(31,58.5,utf8_decode($this->r->nombre));
      $this->text(172,52.5,utf8_decode(1));
      $this->text(159,62.5,utf8_decode("A-01"));
      $this->text(31,66,utf8_decode($this->r->direccion));
      $this->text(31,73.5,utf8_decode($this->r->telefono));
      $this->SetFont('Arial','',8);
      $salto=0;
      foreach ($this->c as $c) {
        if($c->cantidad!="" || $c->cantidad!=null){
          $this->text(13,90+$salto,utf8_decode($c->cantidad));
          $this->text(24,90+$salto,utf8_decode($c->descripcion));
          $this->text(147,90+$salto,utf8_decode("$".number_format($c->precio,2)));
          $this->text(166,90+$salto,utf8_decode($c->iva."%"));
          $this->text(185,90+$salto,utf8_decode("$".number_format($c->importe,2)));
        }
        $salto=$salto+5;
        $this->Ln(6);
      }
      $this->SetFont('Arial','',8);
      $this->text(136,237,utf8_decode("IMPORTE TOTAL"));
      $this->text(186,237,utf8_decode("$".number_format($this->r->importe,2)));
      $this->text(16,249.5,utf8_decode("$".$this->l));
      $this->text(35,260.3,utf8_decode(number_format($this->r->importe,2)));
      $this->text(47,267,utf8_decode("3"));
      //$this->setXY(10,30);
      //$this->multicell(80,7,utf8_decode("DIRECCIÓN: ".$this->r->direccion1),0,"L",false);


	}

}

$pdf = new PDF('P','mm',array(216,279));
$pdf->l=$letras;
$pdf->c=$articulos;
$pdf->r=$compra;
$pdf->fechao = date("d      m     Y", strtotime($pdf->r->fecha));
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Output();
exit();


?>
