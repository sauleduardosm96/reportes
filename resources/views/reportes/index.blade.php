<?php
include(app_path().'\..\public\Fpdf181\fpdf.php');
class PDF extends FPDF
{
	function Header(){
      global $title;
  		$this->SetFont('Arial','B',15);
  		$w = $this->GetStringWidth($title)+20;
  		$this->SetFont('Arial','',9);
      $this->Ln(20);
      $this->cell(0,0,utf8_decode($this->texto),0,0,'C');
      if(isset($this->cliente)){
        $this->Ln(4);
        $this->cell(0,0,utf8_decode($this->cliente->nombre),0,0,'C');
      }
      $this->Ln(4);
      $this->Image('../public/img/dagisa.jpg',85,5,40);
	}

  public function encabezado($w,$header){
		$this->SetFillColor(231,243,243);
		$this->SetTextColor(0);
		$this->SetDrawColor(154,202,195);
		$this->SetFont('','',8);
		$this->SetLineWidth(.1);
		for($i=0;$i<count($header);$i++)
				$this->Cell($w[$i],6,utf8_decode($header[$i]),1,0,'C',true);
		$this->Ln();
		$this->SetFillColor(255,255,255);
		$this->SetDrawColor(255);
		$this->SetTextColor(0);
		$this->SetFont('','',7);
	}

  function FancyTable($header){
	    $w = array(13, 80, 19 ,35, 17, 17,17);
			$this->encabezado($w,$header);
			$fill = false;
			$i=0;
			$j=0;
      $importe=0;
      $pagado=0;
      $pendiente=0;
      $contador=0;
	    foreach($this->remisiones as $row){
						if(($i%35)==0 && ($i>3)){
              $this->Ln(6);
							$this->encabezado($w,$header);
						}

            $contador=$contador+1;
            $fecha1 = new DateTime("NOW");
            $fecha1 = new \DateTime($fecha1->format("Y-m-d"));
            $fecha2 = new DateTime($row->fechafin);
            $diff = $fecha1->diff($fecha2);

            $cont=0;
            $palabra="";

            if($row->pago==1){
              $palabra="Completado.";
            }
            elseif($row->pago==2){
              $palabra="Completado a destiempo.";
            }
            elseif($row->pago==3){
              if($diff->days!=0){
                $palabra="Quedan: ".$diff->days." días.";
              }
              else{
                $palabra="Vence hoy";
              }
            }
            elseif($row->pago==4){
              $palabra="Venció hace: ".$diff->days." días.";
            }


            $this->Cell($w[0],6,$row->idre,'LR',0,'C',$fill);
						$this->Cell($w[1],6,utf8_decode($row->nombre),'LR',0,'C',$fill);
            $this->Cell($w[2],6,$row->fecha,'LR',0,'C',$fill);
            $this->Cell($w[3],6,utf8_decode($palabra),'LR',0,'C',$fill);
            $this->Cell($w[4],6,"$".number_format($row->importe,2),'LR',0,'R',$fill);

            $importe=$importe+$row->importe;
            foreach($this->pagos as $p){
              if($p->id_remision==$row->idre){
                $cont=$cont+$p->cantidad;
              }
            }
            $this->Cell($w[5],6,"$".number_format($cont,2),'LR',0,'R',$fill);
            $pagado=$pagado+$cont;
            $this->Cell($w[6],6,"$".number_format($row->importe-$cont,2),'LR',0,'R',$fill);
            $pendiente=$pendiente+$row->importe-$cont;
		        $this->Ln();
		        $fill = !$fill;

						$i++;
		    }

				$this->Ln();
        $this->Cell($w[0],6,"",'LR',0,'C',false);
        $this->Cell($w[1],6,"",'LR',0,'C',false);
        $aux=0;
        $auxa=0;
        if(!isset($remisionesAlt)){
          $this->Cell($w[2],6,"",'LR',0,'C',false);
          $aux=1;
        }
        $this->Cell($w[2+$aux],6,"Total de movimientos:".$contador,'LR',0,'C',false);
          $can=0;
          $sumim=0;

        if(isset($remisionesAlt)){
          foreach($remisionesAlt as $ra){
             $sumim=$sumim+$ra->importe;
            foreach($pagos as $p){
              if($p->id_remision==$ra->idre){

                  $can=$can+$p->cantidad;

              }
            }
          }
          $this->Cell($w[3+$aux],6,"Saldo inicial: $".number_format($sumim-$can,2),'LR',0,'C',false);
          $auxa=1;
        }
        $this->Cell($w[3+$aux+$auxa],6,"$".number_format($importe,2),'LR',0,'R',false);
        $this->Cell($w[4+$aux+$auxa],6,"$".number_format($pagado,2),'LR',0,'R',false);
        $this->Cell($w[5+$aux+$auxa],6,"$".number_format($pendiente,2),'LR',0,'R',false);
		    $this->Cell(array_sum($w),0,'','T');
				$this->Ln(10);
	}


}

$pdf = new PDF('P','mm',array(216,279));
$header = array('Remisión', 'Cliente', 'Fecha', 'Estado','Importe', 'Pagado', 'Pendiente');
$pdf->remisiones=$remisiones;
$pdf->pagos=$pagos;
$pdf->texto=$texto;
if(isset($cliente)){
  $pdf->cliente=$cliente;
}
if(isset($remisionesAlt)){
  $pdf->remisionesAlt=$remisionesAlt;
}
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->FancyTable($header);
$pdf->Output();
exit();


?>
