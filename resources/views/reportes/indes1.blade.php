@extends('layouts.main')

@section('seccion')

@endsection

@section('titulo')

@endsection

@section('boton')

@endsection

@section('descripcion')

@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection


@section('contenido')
<h4 style="text-align:center">
  <img src="{{asset('img/dagisa.png')}}" height="100px"alt="">
</h4>
<h4>{{$texto}}</h4>
@if(isset($cliente))
  <h4>{{$cliente->nombre}}</h4>
@endif


  <table  class="table text-center">
    <thead class="thead-light">
          <tr style="height:4px" >
            <th scope="col">Número de remisión</th>
            <th scope="col">Cliente</th>
            <th scope="col">Fecha</th>
            <th scope="col">Estado</th>
            <th scope="col">Importe</th>
            <th scope="col">Pagado</th>
            <th scope="col">Pendiente</th>
          </tr>
        </thead>
        <tbody>
          @php
            $importe=0;
            $pagado=0;
            $pendiente=0;
            $contador=0;
          @endphp
          @foreach($remisiones as $r)

              @php
              $contador=$contador+1;
              $fecha1 = new DateTime("NOW");
              $fecha1 = new \DateTime($fecha1->format("Y-m-d"));
              $fecha2 = new DateTime($r->fechafin);
              $diff = $fecha1->diff($fecha2);

              $cont=0;
              $palabra="";
              @endphp

              @if($r->pago==1)
                @php $palabra="Completado."; @endphp
                <tr>
              @elseif($r->pago==2)
                @php $palabra="Completado a destiempo."; @endphp
                <tr>
              @elseif($r->pago==3)
                @if($diff->days!=0)
                  @php $palabra="Quedan: ".$diff->days." días."; @endphp
                @else
                  @php $palabra="Vence hoy"; @endphp
                @endif
                <tr>
              @elseif($r->pago==4)
                @php $palabra="Venció hace: ".$diff->days." días."; @endphp
                <tr>
              @endif
              <th scope="row">{{$r->idre}}</th>
              <td>{{$r->nombre}}</td>
              <td>{{$r->fecha}}</td>
              <td>{{$palabra}}</td>

              <td class="text-right">${{number_format($r->importe,2)}}</td>

              @php $importe=$importe+$r->importe @endphp
              @foreach($pagos as $p)
                @if($p->id_remision==$r->idre)
                  @php
                    $cont=$cont+$p->cantidad;
                  @endphp
                @endif
              @endforeach
              <td class="text-right">${{number_format($cont,2)}}</td>
              @php $pagado=$pagado+$cont @endphp
              <td class="text-right">${{number_format($r->importe-$cont,2)}}</td>
              @php $pendiente=$pendiente+$r->importe-$cont @endphp
            </tr>


          @endforeach
          <tr>
            <th scope="row"></th>
            <td></td>
            @if(!isset($remisionesAlt))

            	<td></td>
            @endif
            <td>Total de movimientos: {{$contador}}</td>
            @php
              $can=0;
              $sumim=0;
            @endphp
            @if(isset($remisionesAlt))
              @foreach($remisionesAlt as $ra)
                @php $sumim=$sumim+$ra->importe; @endphp
                @foreach($pagos as $p)
                  @if($p->id_remision==$ra->idre)
                    @php
                      $can=$can+$p->cantidad;
                    @endphp
                  @endif
                @endforeach
              @endforeach
              <td> {{"Saldo inicial: $".number_format($sumim-$can,2)}}</td>
            @endif
            <td class="text-right">{{"$".number_format($importe,2)}}</td>
            <td class="text-right">{{"$".number_format($pagado,2)}}</td>
            <td class="text-right">{{"$".number_format($pendiente,2)}}</td>
          </tr>
        </tbody>
      </table>
      @endsection
