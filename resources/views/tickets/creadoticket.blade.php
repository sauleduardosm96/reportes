<?php
include(app_path().'\..\public\Fpdf181\fpdf.php');
class PDF extends FPDF
{
	function Header(){
		//$this->Image('../public/img/tickets.jpg',0,0,-300);

    if($this->r->dato31!=""){
  		$this->SetFont('Arial','',7);
      $this->text(15,23,utf8_decode("NO. CLIENTE: ".$this->r->dato31));
      $this->text(70,23,utf8_decode("NO. PAQUETE: ".$this->r->inicial1)."/".$this->r->final1);
      $this->text(15,29,utf8_decode("NOMBRE: ".$this->r->dato41));
      $this->setXY(14,32);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion1),0,"L",false);
			$this->Image('../public/img/logo.png',35,40,40);
    }
		if($this->r->dato32!=""){
  		$this->SetFont('Arial','',7);
      $this->text(122,23,utf8_decode("NO. CLIENTE: ".$this->r->dato32));
      $this->text(177,23,utf8_decode("NO. PAQUETE: ".$this->r->inicial2)."/".$this->r->final2);
      $this->text(122,29,utf8_decode("NOMBRE: ".$this->r->dato42));
      $this->setXY(121,32);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion2),0,"L",false);
			$this->Image('../public/img/logo.png',143,40,40);
    }
    if($this->r->dato33!=""){
			$this->SetFont('Arial','',7);
      $this->text(15,74,utf8_decode("NO. CLIENTE: ".$this->r->dato33));
      $this->text(70,74,utf8_decode("NO. PAQUETE: ".$this->r->inicial3)."/".$this->r->final3);
      $this->text(15,80,utf8_decode("NOMBRE: ".$this->r->dato43));
      $this->setXY(14,83);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion3),0,"L",false);
			$this->Image('../public/img/logo.png',35,91,40);
    }
    if($this->r->dato34!=""){
			$this->SetFont('Arial','',7);
			$this->text(122,74,utf8_decode("NO. CLIENTE: ".$this->r->dato34));
			$this->text(177,74,utf8_decode("NO. PAQUETE: ".$this->r->inicial4)."/".$this->r->final4);
			$this->text(122,80,utf8_decode("NOMBRE: ".$this->r->dato44));
			$this->setXY(121,83);
			$this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion4),0,"L",false);
			$this->Image('../public/img/logo.png',143,91,40);
    }
    if($this->r->dato35!=""){
      $this->SetFont('Arial','',7);
      $this->text(15,125,utf8_decode("NO. CLIENTE: ".$this->r->dato35));
      $this->text(70,125,utf8_decode("NO. PAQUETE: ".$this->r->inicial5)."/".$this->r->final5);
      $this->text(15,131,utf8_decode("NOMBRE: ".$this->r->dato45));
      $this->setXY(14,133);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion5),0,"L",false);
			$this->Image('../public/img/logo.png',35,142,40);
    }
    if($this->r->dato36!=""){
      $this->SetFont('Arial','',7);
      $this->text(122,125,utf8_decode("NO. CLIENTE: ".$this->r->dato36));
      $this->text(177,125,utf8_decode("NO. PAQUETE: ".$this->r->inicial6)."/".$this->r->final6);
      $this->text(122,131,utf8_decode("NOMBRE: ".$this->r->dato46));
      $this->setXY(121,133);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion6),0,"L",false);
			$this->Image('../public/img/logo.png',143,142,40);
    }
		if($this->r->dato37!=""){
      $this->SetFont('Arial','',7);
      $this->text(15,176,utf8_decode("NO. CLIENTE: ".$this->r->dato37));
      $this->text(70,176,utf8_decode("NO. PAQUETE: ".$this->r->inicial7)."/".$this->r->final7);
      $this->text(15,182,utf8_decode("NOMBRE: ".$this->r->dato47));
      $this->setXY(14,184);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion7),0,"L",false);
			$this->Image('../public/img/logo.png',35,193,40);
    }
    if($this->r->dato38!=""){
      $this->SetFont('Arial','',7);
      $this->text(122,176,utf8_decode("NO. CLIENTE: ".$this->r->dato38));
      $this->text(177,176,utf8_decode("NO. PAQUETE: ".$this->r->inicial8)."/".$this->r->final8);
      $this->text(122,182,utf8_decode("NOMBRE: ".$this->r->dato48));
      $this->setXY(121,184);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion8),0,"L",false);
			$this->Image('../public/img/logo.png',143,193,40);
    }
		if($this->r->dato39!=""){
      $this->SetFont('Arial','',7);
      $this->text(15,227,utf8_decode("NO. CLIENTE: ".$this->r->dato39));
      $this->text(70,227,utf8_decode("NO. PAQUETE: ".$this->r->inicial9)."/".$this->r->final9);
      $this->text(15,233,utf8_decode("NOMBRE: ".$this->r->dato49));
      $this->setXY(14,235);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion9),0,"L",false);
			$this->Image('../public/img/logo.png',35,244,40);
    }
    if($this->r->dato310!=""){
      $this->SetFont('Arial','',7);
      $this->text(122,227,utf8_decode("NO. CLIENTE: ".$this->r->dato310));
      $this->text(177,227,utf8_decode("NO. PAQUETE: ".$this->r->inicial10)."/".$this->r->final10);
      $this->text(122,233,utf8_decode("NOMBRE: ".$this->r->dato410));
      $this->setXY(121,235);
      $this->multicell(95,5,utf8_decode("DIRECCIÓN: ".$this->r->direccion10),0,"L",false);
			$this->Image('../public/img/logo.png',143,244,40);
    }
	}
}

$pdf = new PDF('P','mm',array(216,279));
$pdf->r=$request;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Output();
exit();


?>
