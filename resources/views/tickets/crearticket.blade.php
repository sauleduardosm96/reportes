@extends('layouts.main')

@section('seccion')
  Tickets / Generar
@endsection

@section('titulo')
  Nuevo Ticket
@endsection

@section('descripcion')
  Agregar datos de ticket
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')

  <form target="_blank" action="{{route('tickets.store')}}" method="POST">
    <div class="container text-center">
      <div class="row">
          @for($i = 1; $i < 11; $i++)
            <div class="col-6">
              <div class="text-center border border-light p-5" >
                  Ticket {{$i}}
                  <div class="input-group mb-2 mr-sm-2">
                    <input type="text" id="nombre{{$i}}" name="nombre{{$i}}" class="form-control" placeholder="Nombre">
                    <input type="hidden" id="nombreAlt{{$i}}" name="nombreAlt{{$i}}" class="form-control" placeholder="Nombre">
                    <div class="input-group-prepend">
                     <a class="input-group-text btn btn-danger " onclick="limpiarCampo({{$i}})">
                      <i class="fa fa-trash-o"></i>
                     </a>
                    </div>
                  </div>

                  <input type="text" id="direccion{{$i}}" name="direccion{{$i}}" readonly class="form-control mb-2" placeholder="Dirección">
                  <input type="text" id="dato3{{$i}}" name="dato3{{$i}}" readonly class="form-control mb-2" placeholder="E-mail">
                  <input type="text" id="dato4{{$i}}" name="dato4{{$i}}" readonly class="form-control mb-2" placeholder="E-mail">
                  <div class="container text-center">
                    <div class="row">
                      <div class="col-5">
                        <input type="number" id="inicial{{$i}}" name="inicial{{$i}}" class="form-control mb-2" placeholder="Inicial">
                      </div>
                      <div class="col-2">
                        /
                      </div>
                      <div class="col-5">
                        <input type="number" id="final{{$i}}" name="final{{$i}}" class="form-control mb-2" placeholder="Final">
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <script type="text/javascript">
              const ticket{{$i}} = {
                   source: '/ticketsAs',
                   minLength: 2,
                   autoFocus: true,
                   select:function (e,ui) {
                      $("#nombreAlt{{$i}}").val(ui.item.nombre);
                      $("#direccion{{$i}}").val(ui.item.direccion);
                      $("#dato3{{$i}}").val(ui.item.clave);
                      $("#dato4{{$i}}").val(ui.item.nombre);
                      $("#nombre{{$i}}").attr('disabled', true);
                      $("#inicial{{$i}}").attr('required', true);
                      $("#final{{$i}}").attr('required', true);
                   }
              };

              $('#nombre{{$i}}').autocomplete(ticket{{$i}});
                </script>
          @endfor
      </div>
    </div>
    <div class="container text-center">
      <div class="d-flex">
        <div class="mr-auto p-2">
          <button type="submit" class="btn  btn-info text-white">Generar</button>
        </div>
      </div>
    </div>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
  </form>


      <script type="text/javascript">

    function limpiarCampo(id) {
      $("#nombreAlt"+id).val("");
      $("#nombre"+id).val("");
      $("#direccion"+id).val("");
      $("#dato3"+id).val("");
      $("#dato4"+id).val("");
      $("#nombre"+id).attr('disabled', false);
      $("#inicial"+id).attr('required', false);
      $("#final"+id).attr('required', false);
    }

    $(document).on("keypress", 'form', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });
    $(document).on("wheel", "input[type=number]", function (e) {
        $(this).blur();
    });

  </script>
@endsection
