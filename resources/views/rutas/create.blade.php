@extends('layouts.main')

@section('seccion')
  Rutas / Crear
@endsection

@section('titulo')
  Nuevo Ruta
@endsection

@section('descripcion')
  Agregar datos de ruta
@endsection


@section('opciones')
@endsection

@section('contenido')

  <form action="{{route('rutas.store')}}"  enctype="multipart/form-data" method="POST">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
      <label for="exampleForm2">Ruta</label>
      <input type="text" name="ruta" id="ruta" required class="form-control">
    </div>
    <div class="d-flex">
      <div class="mr-auto p-2">
        <button type="submit" class="btn  btn-info text-white">Guardar</button>
        <a href="{{route('rutas.index')}}" class="btn  btn-secondary">Cancelar</a>
      </div>
    </div>
  </form>
@endsection
