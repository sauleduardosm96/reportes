@extends('layouts.main')

@section('seccion')
  Remisiones / Crear
@endsection

@section('titulo')
  Nueva Remisión
@endsection

@section('descripcion')
  Agregar datos de remisión
@endsection



@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')

<div class="container text-center">
  <form action="{{route('remisiones.store')}}" method="POST">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
      <div class="row">

            <div class="col-12">
              <div class="text-center border border-light p-5" >
                  Cliente
                  <div class="input-group mb-2 mr-sm-2">
                    <input type="text" id="nombre" required name="nombre" style="text-transform: uppercase" class="form-control" placeholder="NOMBRE">
                    <input type="hidden" id="nombreAlt" name="nombreAlt" class="form-control" placeholder="Nombre">
                    <div class="input-group-prepend">
                     <a class="input-group-text btn  btn-danger" onclick="limpiarCampo()">
                      <i class="fa fa-trash-o"></i>
                     </a>
                    </div>
                  </div>
                  <input type="text" id="id" name="id" hidden class="form-control mb-2" >
                  <input type="text" id="direccion" name="direccion" readonly class="form-control mb-2" placeholder="DIRECCIÓN">
                  <input type="text" id="telefono" name="telefono" readonly class="form-control mb-2" placeholder="TELÉFONO">
                  <p>Fecha inicial</p>
                  <input type="date" id="fecha" required name="fecha" class="form-control mb-2" placeholder="FECHA">
                  <p>Fecha final</p>
                  <input type="date" id="fechafin" required name="fechafin" class="form-control mb-2" placeholder="FECHA">

              </div>
            </div>
      </div>
      <div class="row">

            <div class="col-12">
              <div class="text-center border border-light p-5" >
                <div class="row">
                  <div class="col">
                    Cantidad
                  </div>
                  <div class="col">
                    Descripción
                  </div>
                  <div class="col">
                    Precio Unitario
                  </div>
                  <div class="col">
                    IVA (%)
                  </div>
                  <div class="col">
                    Importe
                  </div>
                  <br><br>
                </div>
                @for($i = 1; $i < 25; $i++)
                <div class="row">
                  <div class="col">
                    <input type="number" step="0.01"name="cantidad{{$i}}" id="cantidad{{$i}}" onkeyup="calcular({{$i}})" class="form-control" placeholder="Cantidad">
                  </div>
                  <div class="col">
                    <textarea name="descripcion{{$i}}" id="descripcion{{$i}}" class="form-control" placeholder="Descripción"></textarea>
                  </div>
                  <div class="col">
                    <input type="number" step="0.01"name="precio{{$i}}" id="precio{{$i}}" onkeyup="calcular({{$i}})" class="form-control" placeholder="Precio Unitario">
                  </div>
                  <div class="col">
                    <input type="number" step="0.01"name="iva{{$i}}" id="iva{{$i}}" class="form-control" placeholder="IVA">
                  </div>
                  <div class="col">
                    <input type="number" step="0.01"name="importe{{$i}}"  id="importe{{$i}}" class="form-control importes" placeholder="Importe">
                  </div>
                  <br><br><br><br>
                </div>
                @include('remisiones.script')
                @endfor
              </div>
            </div>
      </div>
      <div class="row">

            <div class="col-12">
              <div class="text-center border border-light p-5" >
                <div class="row">
                  <div class="col-sm">
                    <input type="number" class="form-control" placeholder="Cantidad" hidden>
                  </div>
                  <div class="col-sm">
                    <input type="number" class="form-control" placeholder="Cantidad" hidden>
                  </div>
                  <div class="col-sm">
                    <input type="number" class="form-control" placeholder="Cantidad" hidden>
                  </div>
                  <div class="col-sm">
                    <h3>Importe total:</h3>
                  </div>
                  <div class="col-sm">
                    <input type="number" step="0.01"id="importeTotal" name="importeTotal" class="form-control" placeholder="Importe Total" >
                  </div>
                  <br><br>
                </div>
              </div>
            </div>
      </div>
      <div class="d-flex">
        <div class="mr-auto p-2">
          <button type="submit" class="btn  btn-info text-white">Guardar</button>
          <a href="{{route('remisiones.index')}}" class="btn  btn-secondary">Cancelar</a>
        </div>
      </div>
      </form>
    </div>
  @include('remisiones.scripjs')
@endsection
