@extends('layouts.main')

@section('seccion')
  Proveedores / Editar
@endsection

@section('titulo')
  Proveedor: {{$proveedor->nombre}}
@endsection

@section('descripcion')
  Editar datos de proveedor
@endsection



@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')

  <form action="{{route('proveedores.update',$proveedor->id)}}"  method="POST">
    <input type="hidden" name="_method" value="PUT">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

      <div class="form-group">
        <label for="exampleForm2">Nombre</label>
        <input type="text" value="{{$proveedor->nombre}}" name="nombre" id="nombre" class="form-control">
      </div>

      <div class="form-group">
        <label for="exampleForm2">Negocio</label>
        <input type="text" value="{{$proveedor->negocio}}" name="negocio" id="negocio" class="form-control">
      </div>

      <div class="form-group">
        <label for="exampleForm2">Dirección</label>
        <input type="text" value="{{$proveedor->direccion}}" name="direccion" id="direccion" class="form-control">
      </div>

      <div class="form-group">
        <label for="exampleForm2">Colonia</label>
        <input type="text" value="{{$proveedor->colonia}}" name="colonia" id="colonia" class="form-control">
      </div>

      <div class="form-group">
        <label for="exampleForm2">Población</label>
        <input type="text" value="{{$proveedor->poblacion}}" name="poblacion" id="poblacion" class="form-control">
      </div>

      <div class="form-group">
        <label for="exampleForm2">Teléfono</label>
        <input type="text" value="{{$proveedor->telefono}}" name="telefono" id="telefono" class="form-control">
      </div>

      <div class="form-group">
        <label for="exampleForm2">Correo</label>
        <input type="text" value="{{$proveedor->correo}}" name="correo" id="correo" class="form-control">
      </div>

      <div class="d-flex">
        <div class="mr-auto p-2">
          <button type="submit" class="btn  btn-info text-white">Actualizar</button>
          <a href="{{route('proveedores.index')}}" class="btn  btn-secondary">Cancelar</a>
        </div>
  </form>
  <div class="p-2"><button type="button" onclick="$('#formDes').submit();" class="btn  btn-danger">Eliminar</button></div>
</div>
{!! Form::model($proveedor,['route'=>['proveedores.destroy', $proveedor->id], 'method'=>'DELETE', 'id'=>'formDes','onsubmit'=>'return confirm("¿Seguro que desea eliminarlo?")']) !!}
{!!Form::close()!!}
<script type="text/javascript">
function limpiarCampo() {
  $("#ruta").val("");
  $("#ruta_id").val("");
  $("#ruta").attr('required', true);
  $("#ruta").attr('disabled', false);
}
const ruta = {
     source: '/rutasAs',
     minLength: 2,
     autoFocus: true,
     select:function (e,ui) {
        $("#ruta").val(ui.item.ruta);
        $("#ruta_id").val(ui.item.id);
        $("#ruta").attr('disabled', true);
     }
};

$('#ruta').autocomplete(ruta);
</script>
@endsection
