@extends('layouts.main')

@section('seccion')
  Laboratorios / Crear
@endsection

@section('titulo')
  Nuevo Laboratorio
@endsection

@section('descripcion')
  Agregar datos de laboratorio
@endsection


@section('opciones')
@endsection

@section('contenido')

  <form action="{{route('laboratorios.store')}}"  enctype="multipart/form-data" method="POST">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
      <label for="exampleForm2">Laboratorio</label>
      <input type="text" name="laboratorio" id="laboratorio" required class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleForm2">Logo del laboratorio</label>
      <input type="file" name="link_archivo" id="link_archivo" required type="file" accept="image/*" class="form-control">
    </div>
    <div class="d-flex">
      <div class="mr-auto p-2">
        <button type="submit" class="btn  btn-info text-white">Guardar</button>
        <a href="{{route('laboratorios.index')}}" class="btn  btn-secondary">Cancelar</a>
      </div>
    </div>
  </form>
@endsection
