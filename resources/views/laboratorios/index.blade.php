@extends('layouts.main')

@section('seccion')
  Laboratorios
@endsection

@section('titulo')
  Gestor de laboratorios
@endsection

@section('boton')
  <a href="{{route('laboratorios.create')}}" class="btn  btn-info text-white"><i class="fa fa-plus"></i> Registrar nuevo laboratorio</a>
@endsection

@section('descripcion')
  Panel de control para gestionar los laboratorios
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection


@section('contenido')
{{Form::open(['route'=>'laboratorios.index','method'=>'GET'])}}
  @include('layouts.busqueda')
{{Form::close()}}
  <table class="table table-hover table-outline text-center">
    <thead class="thead-light">
      <tr>
        <th scope="col">Laboratorio</th>
        <th scope="col">Logo</th>
        <th scope="col">Editar</th>
      </tr>
    </thead>
    <tbody>
      @foreach($laboratorios as $c)
        <tr>
          <td>{{$c->laboratorio}}</td>
          <td>
            <img height="30px" src="{{asset('storage'.$c->ruta)}}" alt=""></td>
          <td>
            <a class="btn  btn-info text-white" href="{{route('laboratorios.edit',$c->id)}}"><i class="icon-pencil "></i></a>
          </td>
        </tr>
      @endforeach

    </tbody>
  </table>
  {!!$laboratorios->appends(Request::only(['laboratorio']))->render()!!}
  <script type="text/javascript">
    function cerrar() {
        $('#not').remove();
    }
  </script>

@endsection
