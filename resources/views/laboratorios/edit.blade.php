@extends('layouts.main')

@section('seccion')
  Laboratorios / Editar
@endsection

@section('titulo')
  Laboratorio: {{$laboratorio->laboratorio}}
@endsection

@section('descripcion')
  Editar datos de laboratorio
@endsection



@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')

  <form action="{{route('laboratorios.update',$laboratorio->id)}}" enctype="multipart/form-data"  method="POST">
    <input type="hidden" name="_method" value="PUT">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

      <div class="form-group">
        <label for="exampleForm2">Nombre</label>
        <input type="text" value="{{$laboratorio->laboratorio}}" required name="laboratorio" id="laboratorio" class="form-control">
      </div>
      <div class="form-group">
        <label for="exampleForm2">Logo del laboratorio</label>
        <input type="file" name="link_archivo" id="link_archivo"  type="file" accept="image/*" class="form-control">
      </div>
      <div class="d-flex">
        <div class="mr-auto p-2">
          <button type="submit" class="btn  btn-info text-white">Actualizar</button>
          <a href="{{route('laboratorios.index')}}" class="btn  btn-secondary">Cancelar</a>
        </div>
  </form>
  <div class="p-2"><button type="button" onclick="$('#formDes').submit();" class="btn  btn-danger">Eliminar</button></div>
</div>
{!! Form::model($laboratorio,['route'=>['laboratorios.destroy', $laboratorio->id], 'method'=>'DELETE', 'id'=>'formDes','onsubmit'=>'return confirm("¿Seguro que desea eliminarlo?")']) !!}
{!!Form::close()!!}
@endsection
