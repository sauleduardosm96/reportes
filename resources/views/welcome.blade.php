<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Nova Social Media Consultoría</title>
  <!-- MDB icon -->
  <link rel="icon" href="img/ico.png" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="css/mdb.min.css">
  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="css/style.css">
  <style type="text/css">
      #myBtn {
      display: none;
      position: fixed;
      bottom: 20px;
      right: 30px;
      z-index: 99;
      font-size: 18px;
      outline: none;
      background-color: #6a3096;
      color: white;
      cursor: pointer;
      padding: 15px;
      height: 60px;
      width: 60px;
      border-radius: 50%;
      border: 0px solid black;
    }

    #myBtn:hover {
      background-color: #6a3096;
    }
    /* Navigation*/
    html {
      scroll-behavior: smooth;
    }
    .navbar {
      background-color: transparent;
    }
    .scrolling-navbar {
      transition: background .5s ease-in-out, padding .5s ease-in-out;
    }
    .top-nav-collapse {
      background-color: #1a1a1a;
    }
    footer.page-footer {
      background-color: #34456a;
    }
    @media only screen and (max-width: 768px) {
      .navbar {
        background-color: #000000;
      }
    }

    /* Necessary for full page carousel*/
    html,
    body,
    .view {
      height: 100%;
    }

    /* Carousel*/
    .carousel,
    .carousel-item,
    .carousel-item.active {
      height: 100%;
    }
    .carousel-inner {
      height: 100%;
    }
    .carousel .carousel-item video {
      min-width: 100%;
      min-height: 100%;
      width: auto;
      height: auto;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
  </style>
</head>
<body>

<button onclick="topFunction()" id="myBtn" class="waves-effect waves-light" title="Go to top"><i class="fas fa-long-arrow-alt-up"></i></button>
  <!--Navbar-->
      <nav class="navbar navbar-expand-lg navbar-dark scrolling-navbar fixed-top">

        <!-- Navbar brand -->
        <a class="navbar-brand" href="#">
          <img src="img/logo.png" height="60" alt="mdb logo">
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="basicExampleNav">

          <!-- Links -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Inicio

              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#quienes">¿Quiénes somos?</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#nuestros">Nuestros planes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#clientes">Nuestros clientes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contacto">Contacto</a>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto nav-flex-icons">
            <li class="nav-item">
              <a href="https://wa.me/525568796534?text=Hola,%20me%20gustar%C3%ADa%20recibir%20informaci%C3%B3n" target="_blank" class="nav-link waves-effect waves-light">
                <i class="fab fa-whatsapp"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="https://www.facebook.com/Novasocialmedia2020" target="_blank" class="nav-link waves-effect waves-light">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="https://www.instagram.com/nova.social.media/" target="_blank" class="nav-link waves-effect waves-light">
                <i class="fab fa-instagram"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="mailto:contacto@novasocialmedia.com" class="nav-link waves-effect waves-light">
                <i class="far fa-envelope"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="tel:55 6879 6534" class="nav-link waves-effect waves-light">
                <i class="fas fa-phone"></i>
              </a>
            </li>
          </ul>
        </div>
        <!-- Collapsible content -->

      </nav>


      <!--Carousel Wrapper-->
      <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel" >
          <!--Indicators-->
          <ol class="carousel-indicators">
              <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-2" data-slide-to="1"></li>
          </ol>
          <!--/.Indicators-->

          <!--Slides-->
          <div class="carousel-inner" role="listbox">

              <!--First slide-->
              <div class="carousel-item active">
                  <!--Mask-->
                  <div class="view">
                    <!--Video source-->
                    <video autoplay playsinline muted>
                        <source src="video/nova.mp4" type="video/mp4" />
                    </video>
                    <!-- Carousel content -->
                    <!--<div class="full-bg-img flex-center mask rgba-indigo-light white-text">
                      <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                        <li>
                          <h1 class="font-weight-bold text-uppercase">Portada de ejemplo</h1>
                        </li>
                        <li>
                          <p class="font-weight-bold text-uppercase py-4">Datos de la portada</p>
                        </li>
                        <li class="list-inline-item">
                          <a  href="#" class="btn btn-unique btn-lg btn-rounded mr-0">Botón 1</a>
                        </li>
                        <li class="list-inline-item">
                          <a  href="#" class="btn btn-cyan btn-lg btn-rounded ml-0">Botón 2</a>
                        </li>
                      </ul>
                    </div>-->
                  </div>
                  <!--/Mask-->
              </div>
              <!--/First slide-->

              <!--Second slide-->
              <style media="screen">
                .view {
                  height: 100%;
                }

                .carousel-item.active {
                  height: 100%;
                }
                .carousel-inner {
                  height: 100%;
                }
                .patron {
                  background-image: url("img/portada.jpg");
                  background-repeat: no-repeat;
                  background-size: cover;
                  background-position: center center;
                }
                .patron2 {
                  background-image: url("img/portada2.png");
                  background-repeat: no-repeat;
                  background-size: cover;
                  background-position: center center;
                }
              </style>
              <div class="carousel-item text-center ">
                <div class="view">
                  <div class="full-bg-img flex-center mask patron d-md-block  rgba-black-strong white-text">
                    <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                    </ul>
                  </div>
                  <div class="full-bg-img flex-center mask patron2 d-md-none rgba-black-strong white-text">
                    <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                      <li class="d-md-none">
                        <h2 style="text-shadow: 2px 2px 5px black;"class="font-weight-bold text-uppercase">
                          Multimedia
                          <br><br>Redes sociales
                          <br><br>Diseño web
                        </h2>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="carousel-caption d-md-none" >
                  <h2 style="text-shadow: 0 0 25px white;"><i class="fab fa-instagram"></i>&nbsp;&nbsp;<i class="fab fa-facebook-f"></i><strong>&nbsp;&nbsp;&nbsp;nova</strong></h2>
                  <br>
                </div>
              </div>
              <!--/Second slide-->
          </div>
          <!--/.Slides-->

          <!--Controls-->
          <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
          </a>
          <a  class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
          </a>
          <!--/.Controls-->
      </div>
      <!--/.Carousel Wrapper-->
      <br id="quienes">
      <br>
      <!-- Section: Features v.2 -->
      <section class="my-5">
        <h2 class="h1-responsive font-weight-bold text-center my-5">¿Quiénes somos?</h2>
        <div class="container lead text-body text-center  w-responsive mx-auto mb-5">
          <div class="row">

            <div class="col-4 d-none d-lg-block">
              <img width="100%" src="img/quienes.jpg" >
            </div>
            <div class="col-sm">
              <p class="lead text-body text-center w-responsive mx-auto mb-4">Somos una consultoría especializada en Social Media y
                Posicionamiento de Marcas en Internet; un socio de negocios que te ayudará a generar impacto en redes sociales a través de
                la publicación de contenido audiovisual, diseñando campañas publicitarias y construyendo una reputación positiva con las herramientas tecnológicas más novedosas en el mercado.</p>
              <p class="lead text-body text-center w-responsive mx-auto mb-4">Nuestra metodología de posicionamiento te permitirá llegar a muchas más
                personas, facilitando la retroalimentación para realizar una planificación adecuada de nuevas estrategias
                que propicien el crecimiento a largo plazo de tu negocio.</p>
            </div>
          </div>
          <!--<div class="row" >
            <div class="col-12">
              <p class="lead text-body text-center w-responsive mx-auto mb-4">Somos una consultoría especializada en Social Media y
                Posicionamiento de Marcas en Internet; un socio de negocios que te ayudará a generar impacto en redes sociales a través de
                la publicación de contenido audiovisual, diseñando campañas publicitarias y construyendo una reputación positiva con las herramientas tecnológicas más novedosas en el mercado.</p>
              <p class="lead text-body text-center w-responsive mx-auto mb-4">Nuestra metodología de posicionamiento te permitirá llegar a muchas más
                personas, facilitando la retroalimentación para realizar una planificación adecuada de nuevas estrategias
                que propicien el crecimiento a largo plazo de tu negocio.</p>
            </div>
          </div>-->
        </div>

        <!--<div class="container lead text-body text-center  w-responsive mx-auto mb-5">
          <div class="row">
            <div class="col-sm">
              Community manager.
            </div>
            <div class="col-sm">
              Branding y diseño gráfico.
            </div>
            <div class="col-sm">
              Analisis de datos.
            </div>
          </div>
        </div>-->

      </section>
      <br id="nuestros">
      <br>
      <!-- Section: Features v.2 -->
      <section class="my-5">

        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5">Nuestros planes</h2>
        <!-- Section description -->
        <p class="lead text-body text-center w-responsive mx-auto mb-5">Contamos con una amplia gama de servicios que te permitirán posicionar tu marca a través de diversas herramientas.</p>

        <div class="container lead text-body text-center  w-responsive mx-auto mb-5">
          <div class="row">
            <div class="col-sm" style="margin-bottom:30px">
                            <!-- Card Narrower -->
              <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                  <img  class="card-img-top" src="img/basico.jpg" alt="Card image cap">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade">

                  <!-- Label -->
                  <h5 class="purple-text pb-2 pt-1"><i class="fas fa-cubes"></i> Paquete</h5>
                  <!-- Title -->
                  <h4 class="font-weight-bold card-title">Básico</h4>
                  <!-- Text -->
                  <p class="card-text text-left">
                    ►Estrategia de comunicación<br>
                    ►Calendario de publicaciones<br>
                    ►Creación o actualización de página de Facebook<br>
                    ►2 publicaciones mensuales promocionadas (con imagen de stock)<br>
                    ►2 publicaciones semanales sin promocionar (con imagen de stock)<br>
                    ►Reporte Mensual<br><br><br><br>
                  </p>
                  <!-- Button -->
                  <h3 class="font-weight-bold purple-text">$1,500</h3>

                </div>

              </div>
              <!-- Card Narrower -->
            </div>
            <div class="col-sm" style="margin-bottom:30px">
                            <!-- Card Narrower -->
              <div class="card  z-depth-5 border border-secondary card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                  <img  class="card-img-top" src="img/pyme.jpeg" alt="Card image cap">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>

                <!-- Card content -->
                <div class="card-body  card-body-cascade">

                  <!-- Label -->
                  <h5 class="purple-text pb-2 pt-1"><i class="fas fa-cubes"></i> Paquete</h5>
                  <!-- Title -->
                  <h4 class="font-weight-bold  card-title">PyME</h4>
                  <!-- Text -->
                  <p class="card-text text-left">
                    ►Estrategia de comunicación<br>
                    ►Calendario de publicaciones<br>
                    ►Creación o actualización de página de Facebook<br>
                    ►4 publicaciones mensuales promocionadas (con imagen)<br>
                    ►3 publicaciones semanales sin promocionar (con imagen)<br>
                    ►$400 de publicidad en Facebook<br>
                    ►Reporte Mensual
                    <br><br><br>
                  </p>
                  <!-- Button -->
                  <h3 class="font-weight-bold purple-text">$2,500</h3>

                </div>

              </div>
              <!-- Card Narrower -->
            </div >
            <div class="col-sm" style="margin-bottom:30px">
                            <!-- Card Narrower -->
              <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                  <img  class="card-img-top" src="img/empresarial.jpeg" alt="Card image cap">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade">

                  <!-- Label -->
                  <h5 class="purple-text pb-2 pt-1"><i class="fas fa-cubes"></i> Paquete</h5>
                  <!-- Title -->
                  <h4 class="font-weight-bold card-title">Empresarial</h4>
                  <!-- Text -->
                  <p class="card-text text-left">
                    ►Estrategia de comunicación<br>
                    ►Calendario de publicaciones<br>
                    ►Creación o actualización de página de Facebook y red social a elegir (Instagram o Twitter)<br>
                    ►4 publicaciones mensuales promocionadas (con imagen)<br>
                    ►4 publicaciones semanales sin promocionar (con imagen)<br>
                    ►$800 de publicidad en Facebook y/o Google Adwords<br>
                    ►Reporte Mensual
                  </p>
                  <!-- Button -->
                  <h3 class="font-weight-bold purple-text">$4,000</h3>

                </div>

              </div>
              <!-- Card Narrower -->
            </div>

          </div>
        </div>
        <div class="container lead text-body text-center  w-responsive mx-auto mb-5">
          <div class="row">
            <div class="col-sm" style="margin-bottom:30px">
                            <!-- Card Narrower -->
              <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                  <img  class="card-img-top" src="img/estrategico.jpeg" alt="Card image cap">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade">

                  <!-- Label -->
                  <h5 class="purple-text pb-2 pt-1"><i class="fas fa-cubes"></i> Paquete</h5>
                  <!-- Title -->
                  <h4 class="font-weight-bold card-title">Estratégico</h4>
                  <!-- Text -->
                  <p class="card-text text-left">
                    ►Estrategia de comunicación<br>
                    ►Video promocional 30 - 60 segundos<br>
                    ►Calendario de publicaciones<br>
                    ►Creación o actualización de página de Facebook y red social a elegir (Instagram o Twitter)<br>
                    ►6 publicaciones mensuales promocionadas (con imagen o con el video promocional)<br>
                    ►5 publicaciones semanales sin promocionar (con imagen)<br>
                    ►$1,500 de publicidad en Facebook y/o Google Adwords<br>
                    ►Reporte Mensual<br><br><br>
                  </p>
                  <!-- Button -->
                  <h3 class="font-weight-bold purple-text">$7,000</h3>

                </div>

              </div>
              <!-- Card Narrower -->
            </div>
            <div class="col-sm" style="margin-bottom:30px">
                            <!-- Card Narrower -->
              <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                  <img  class="card-img-top" src="img/premium.jpeg" alt="Card image cap">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade">

                  <!-- Label -->
                  <h5 class="purple-text pb-2 pt-1"><i class="fas fa-cubes"></i> Paquete</h5>
                  <!-- Title -->
                  <h4 class="font-weight-bold card-title">Premium</h4>
                  <!-- Text -->
                  <p class="card-text text-left">
                    ►Estrategia de comunicación<br>
                    ►2 Videos promocionales 30 - 60 segundos<br>
                    ►Calendario de publicaciones<br>
                    ►Creación o actualización de página de Facebook y red social a elegir (Instagram o Twitter)<br>
                    ►6 publicaciones mensuales promocionadas (con imagen o con el video promocional)<br>
                    ►6 publicaciones semanales sin promocionar (con imagen)<br>
                    ►$2,500 de publicidad en Facebook y/o Google Adwords<br>
                    ►Reporte Mensual<br><br><br>
                  </p>
                  <!-- Button -->
                  <h3 class="font-weight-bold purple-text">$10,000</h3>

                </div>

              </div>
              <!-- Card Narrower -->
            </div>
            <div class="col-sm" style="margin-bottom:30px">
                            <!-- Card Narrower -->
              <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                  <img  class="card-img-top" src="img/express.jpeg" alt="Card image cap">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade">

                  <!-- Label -->
                  <h5 class="purple-text pb-2 pt-1"><i class="fas fa-cubes"></i> Paquete</h5>
                  <!-- Title -->
                  <h4 class="font-weight-bold card-title">Express</h4>
                  <!-- Text -->
                  <p class="card-text text-left">
                    ►Posicionamiento inmediato<br>
                    ►Estrategia de comunicación<br>
                    ►4 Videos promocionales 30 - 60 segundos<br>
                    ►Calendario de publicaciones<br>
                    ►Creación o actualización de página de Facebook, Instagram y Twitter<br>
                    ►Se cubren 2 eventos de forma presencial (máximo 1 hora)<br>
                    ►6 publicaciones mensuales promocionadas (con imagen o video)<br>
                    ►6 publicaciones semanales sin promocionar (con imagen o video)<br>
                    ►$10,000 de publicidad en Facebook y/o Google Adwords<br>
                    ►Reporte Mensual<br>
                  </p>
                  <!-- Button -->
                  <h3 class="font-weight-bold purple-text">$30,000</h3>

                </div>

              </div>
              <!-- Card Narrower -->
            </div>


          </div>
        </div>
        <div class="container lead text-body text-center  w-responsive mx-auto mb-5">
          <div class="row">
            <div class="col-sm">

            </div>
            <div class="col-sm">
                            <!-- Card Narrower -->
              <div class="card card-cascade narrower">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                  <img  class="card-img-top" src="img/otros.jpg" alt="Card image cap">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade">

                  <!-- Label -->
                  <h5 class="purple-text pb-2 pt-1"><i class="fas fa-cubes"></i> Extras</h5>
                  <!-- Title -->
                  <h4 class="font-weight-bold card-title">Servicios Adicionales</h4>
                  <!-- Text -->
                  <p class="card-text text-left">
                    ►Video institucional (1-2 minutos) desde $4,000<br>
                    ►Video promocional desde $4,000<br>
                    ►Identidad corporativa $4,000<br>
                    ►Diseño de logotipo $900<br>
                    ►Diseño gráfico (Diferentes precios)<br>
                    ►Creación de página de internet:<br>
                      -1 Landing Page de 3 secciones (1 año) $4,500<br>
                      -3 secciones (1 año) $6,000<br>
                      -5 secciones (1 año) $7,000<br>
                      -eCommerce(1 año) $15,000<br><br>
                  </p>
                  <!-- Button -->

                </div>

              </div>
              <!-- Card Narrower -->
            </div>
            <div class="col-sm">

            </div>


          </div>
        </div>
      </section>

      <br id="clientes">
      <br>
      <section class="my-5" >

        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5" >Nuestros clientes</h2>
        <!-- Section description -->
        <p class="lead text-body text-center w-responsive mx-auto mb-5">Nuestros clientes son lo más importante para nosotros, les brindamos nuestro respaldo las 24 horas del día, los 365 días del año.</p>

        <div class="container lead text-body text-center  w-responsive mx-auto mb-5">
          <div class="row">
            <div class="col-sm">
              <img src="img/muchitos.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
  <br><br>
  <img src="img/conceptos3.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
            </div>
            <div class="col-sm">
              <img src="img/merka.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
  <br><br>
  <img src="img/canela.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
            </div>
            <div class="col-sm">
              <img src="img/chango.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
  <br><br>
  <img src="img/colmena.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
            </div>
            <div class="col-sm">
              <img src="img/laredo.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
  <br><br>
  <img src="img/balam.jpg" alt="thumbnail" class="img-thumbnail"
  style="width: 250px">
            </div>
          </div>
        </div>

      </section>
      <br id="contacto">
      <br>

      <section class="my-5" >

        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5" >Contacto</h2>
        <!-- Section description -->
        @if (session('info'))
          <div class="alert text-center alert-primary alert-dismissible fade show" role="alert">
            <h3>Se ha enviado su correo, lo contactaremos a la brevedad</h3>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif


        <p class="lead text-body text-center w-responsive mx-auto mb-5">Si quieres recibir información acerca de nuestros planes, puedes ingresar tus datos en el siguiente formulario y nosotros te responderemos cuanto antes o también puedes marcarnos al teléfono <strong><a href="tel:55 6879 6534">5568796534</a></strong> o envíanos un Whatsapp.</p>

        <div class="container lead text-body text-center  w-responsive mx-auto mb-5">
          <div class="row">
            <div class="col-3">
            </div>
            <div class="col-sm">
                          <!-- Default form contact -->
              <form class="text-center border border-light p-5" method="post" action="/contacto">
                  @csrf
                  <p class="h4 mb-4">Contáctanos</p>

                  <!-- Name -->
                  <input type="text" name="nombre" required id="defaultContactFormName" class="form-control mb-4" placeholder="Nombre">

                  <!-- Email -->
                  <input type="email" name="email" required id="defaultContactFormEmail" class="form-control mb-4" placeholder="E-mail">

                  <input type="number" name="telefono" id="telefono" required class="form-control mb-4" placeholder="Número telefónico">



                  <!-- Message -->
                  <div class="form-group">
                      <textarea class="form-control rounded-0" name="mensaje" required id="exampleFormControlTextarea2" rows="3" placeholder="Mensaje"></textarea>
                  </div>

                  <!-- Send button -->
                  <button class="btn text-white btn-block" type="submit" style="background-color:#6a3096">Enviar</button>

              </form>
              <!-- Default form contact -->
            </div>
            <div class="col-3">
            </div>
          </div>
        </div>

      </section>
      <br>
      <!-- Footer -->
      <!-- Footer -->
      <footer class="page-footer font-small elegant-color-dark">

          <div class="container text-center py-5">
            <div class="row">
              <div class="col-sm">

              </div>
              <div class="col-sm text-center">
                <!-- Facebook -->
                 <a href="https://wa.me/525568796534?text=Hola,%20me%20gustar%C3%ADa%20recibir%20informaci%C3%B3n" target="_blank"  class="fb-ic" >
                   <i class="fab fa-whatsapp fa-lg white-text fa-2x"> </i>
                 </a>
                 <a href="https://www.facebook.com/Novasocialmedia2020" target="_blank"  class="fb-ic" style="margin-left:10%">
                   <i class="fab fa-facebook-f fa-lg white-text fa-2x"> </i>
                 </a>
                 <a href="https://www.instagram.com/nova.social.media/" target="_blank"  class="fb-ic" style="margin-left:10%">
                   <i class="fab fa-instagram fa-lg white-text fa-2x"> </i>
                 </a>
                 <a href="mailto:contacto@novasocialmedia.com"  class="fb-ic" style="margin-left:10%">
                   <i class="far fa-envelope fa-lg white-text fa-2x"> </i>
                 </a>
                 <a href="tel:55 6879 6534" class="fb-ic" style="margin-left:10%">
                   <i class="fas fa-phone fa-lg white-text fa-2x"></i>
                 </a>
              </div>
              <div class="col-sm">

              </div>
            </div>
          </div>

        <div class="footer-copyright text-center py-3">© 2020 Copyright:
          <a href="/"> novasocialmedia.com</a>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->

  <!-- Copyright -->

</footer>
<!-- Footer -->
      <!-- Section: Features v.2 -->
  <!-- jQuery -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>
  <script>
    $(document).ready(function(){
      $('.carousel').carousel({
        interval: 5000
      })
    });


    //Get the button
    var mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
      } else {
        mybutton.style.display = "none";
      }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
</script>
</body>
</html>
