<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>DAGISA V1.0</title>
  <link rel="stylesheet" type="text/css" href=" {{asset('css/jquery-ui.min.css')}} ">
  <link rel="stylesheet" type="text/css" href=" {{asset('css/jquery-ui.structure.min.css')}} ">
  <link rel="stylesheet" type="text/css" href=" {{asset('css/jquery-ui.theme.min.css')}} ">
  <!--
   <script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
   Bootstrap tooltips -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

  <script type="text/javascript" src="{{asset('js/jquery-1.12.4.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
  <link href="{{asset('css/icon.css')}}" rel="stylesheet">



  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
  <!-- Bootstrap core CSS -->
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Material Design Bootstrap -->
  <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">




</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark primary-color">
  <a class="navbar-brand" href="#">DAGISA V1.0</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{route('clientes.index')}}">Clientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('remisiones.index')}}">Remisiones</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('tickets.index')}}">Tickets</a>
      </li>

    </ul>
  </div>
</nav>
