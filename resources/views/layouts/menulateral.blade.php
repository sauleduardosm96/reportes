<div class="sidebar">
  <nav class="sidebar-nav c-sidebar-dark c-sidebar-show">
    <ul class="nav">
      <li class="nav-title">Menu principal</li>
        <li class="nav-item">
          <a class="nav-link font-weight-bold" href="{{route('usuarios.index')}}">
            <i class="nav-icon fa fa-users"></i> Usuarios</a>
        </li>
        <li class="nav-item">
          <a class="nav-link font-weight-bold" href="{{route('clientes.index')}}">
            <i class="nav-icon fa fa-address-book-o"></i> Clientes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link font-weight-bold" href="{{route('remisiones.index')}}">
            <i class="nav-icon fa fa-file-pdf-o"></i> Remisiones</a>
        </li>
        <li class="nav-item">
          <a class="nav-link font-weight-bold" href="{{route('tickets.index')}}">
            <i class="nav-icon fa fa-ticket"></i> Tickets</a>
        </li>
        <li class="nav-item">
          <a class="nav-link font-weight-bold" href="{{route('proveedores.index')}}">
            <i class="nav-icon fa fa-handshake-o"></i> Proveedores</a>
        </li>
        <li class="nav-item">
          <a class="nav-link font-weight-bold" href="{{route('compras.index')}}">
            <i class="nav-icon fa fa-shopping-cart"></i> Compras</a>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle font-weight-bold" href="#">
            <i class="nav-icon fa fa-book"></i>Catalogos
            </a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="{{route('laboratorios.index')}}"><i class="nav-icon fa fa-flask"></i>Laboratorios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('medicamentos.index')}}"><i class="nav-icon fa fa-medkit"></i>Medicamentos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('catalogos')}}"><i class="nav-icon fa fa-print"></i>Imprimir catálogo</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('rutas.index')}}"><i class="nav-icon fa fa-car"></i>Rutas</a>
            </li>
          </ul>
      </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle font-weight-bold" href="#">
            <i class="nav-icon icon-list"></i>Reportes
            </a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" data-toggle="modal" data-target="#exampleModalCenter"><i class="nav-icon fa fa-file-pdf-o"></i>Saldo final clientes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="modal" data-target="#exampleModalProveedores"><i class="nav-icon fa fa-file-pdf-o"></i>Saldo final proveedores</a>
            </li>
          </ul>
      </li>
    </ul>
  </nav>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      {!!Form::open(['route'=> 'reporteClientes','files'=>'false','id'=>'formCreate','target'=>'_blank'])!!}
      <div class="modal-header">
        <h5 class="modal-title" id="modalInsercionCenterTitle">Reporte de saldos clientes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
            <div class="input-group mb-2 mr-sm-2">
              <div class="alert alert-info" role="alert">
                Si deja el campo de NOMBRE vacío, el reporte hará un acumulado de todos sus clientes
              </div>
              <input type="text" id="nombreM" name="nombre" style="text-transform: uppercase" class="form-control" placeholder="NOMBRE">
              <input type="text" id="idM" hidden name="id"  class="form-control mb-2" >
              <div class="input-group-prepend">
               <a class="input-group-text btn  btn-danger" onclick="limpiarCampoM()">
                <i class="fa fa-trash-o"></i>
               </a>
              </div>
            </div>
            <br>
            <div class="input-group date">
              <input type="date" name="fecha" data-toggle="tooltip" data-placement="top" title="Fecha inicial" required="required" placeholder="Fecha inicial" autocomplete="off" id="datepicker1" class="datepicker form-control"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
              <input type="date" name="fechafin" data-toggle="tooltip" data-placement="top" title="Fecha final" required="required" placeholder="Fecha final" autocomplete="off" id="datepicker2" class="datepicker form-control"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
            <br>
            <select required name="estado" class="custom-select">
              <option value="1">Pagadas</option>
              <option value="2">Pagadas a destiempo</option>
              <option value="3">En tiempo</option>
              <option value="4">No pagadas</option>
              <option value="5">Todas</option>
            </select>

      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-info text-white" >Generar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModalProveedores" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      {!!Form::open(['route'=> 'reporteProveedores','files'=>'false','id'=>'formCreate','target'=>'_blank'])!!}
      <div class="modal-header">
        <h5 class="modal-title" id="modalProveedoresCenterTitle">Reporte de saldos proveedores</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
            <div class="input-group mb-2 mr-sm-2">
              <div class="alert alert-info" role="alert">
                Si deja el campo de NOMBRE vacío, el reporte hará un acumulado de todos sus proveedores
              </div>
              <input type="text" id="nombreP" name="nombre" style="text-transform: uppercase" class="form-control" placeholder="NOMBRE">
              <input type="text" id="idP" hidden name="id"  class="form-control mb-2" >
              <div class="input-group-prepend">
               <a class="input-group-text btn  btn-danger" onclick="limpiarCampoM()">
                <i class="fa fa-trash-o"></i>
               </a>
              </div>
            </div>
            <br>
            <div class="input-group date">
              <input type="date" name="fecha" data-toggle="tooltip" data-placement="top" title="Fecha inicial" required="required" placeholder="Fecha inicial" autocomplete="off" id="datepicker1" class="datepicker form-control"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
              <input type="date" name="fechafin" data-toggle="tooltip" data-placement="top" title="Fecha final" required="required" placeholder="Fecha final" autocomplete="off" id="datepicker2" class="datepicker form-control"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
            <br>
            <select required name="estado" class="custom-select">
              <option value="1">Pagadas</option>
              <option value="2">Pagadas a destiempo</option>
              <option value="3">En tiempo</option>
              <option value="4">No pagadas</option>
              <option value="5">Todas</option>
            </select>

      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-info text-white" >Generar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<style media="screen">
  .ui-autocomplete {
    z-index: 5000;
  }
</style>
<script type="text/javascript">
  function limpiarCampoM() {
    $("#nombreM").val("");
    $("#idM").val("");
    $("#nombreM").attr('disabled', false);
    $("#nombreP").val("");
    $("#idP").val("");
    $("#nombreP").attr('disabled', false);
  }
  const clienteM = {
       source: '/ticketAs',
       minLength: 2,
       autoFocus: true,
       select:function (e,ui) {

          $("#idM").val(ui.item.id);
          $("#nombreM").attr('disabled', true);
       }
  };

  $('#nombreM').autocomplete(clienteM);
  $( "#nombreM" ).autocomplete( "option", "appendTo", ".eventInsForm" );
  const proveedorM = {
       source: '/ticketPAs',
       minLength: 2,
       autoFocus: true,
       select:function (e,ui) {

          $("#idP").val(ui.item.id);
          $("#nombreP").attr('disabled', true);
       }
  };

  $('#nombreP').autocomplete(proveedorM);
  $( "#nombreP" ).autocomplete( "option", "appendTo", ".eventInsForm" );
</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
