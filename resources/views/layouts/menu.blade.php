

  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="/">
        <img class="navbar-brand-full" src="{{asset('pan/img/brand/logo.png')}}" width="120" alt="SICA">
        <img class="navbar-brand-minimized" src="{{asset('pan/img/brand/sygnet.png')}}" width="30" height="30" alt="SICA">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
          <a class="nav-link" href="/">Inicio</a>
        </li>

        <li class="nav-item px-3">
          <a class="nav-link" href="{{route('usuarios.index')}}">Usuarios</a>
        </li>


      </ul>
      <ul class="nav navbar-nav ml-auto" style="margin-right:1.5%">
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            {{Auth::user()->name}}<img class="img-avatar" src="{{asset('pan/img/avatars/6.png')}}" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Ajustes</strong>
            </div>
            <a class="dropdown-item" href="{{route('usuarios.index')}}">
              <i class="fa fa-users"></i> Usuarios
            </a>
            <a class="dropdown-item" href="{{route('cambiar')}}">
              <i class="fa fa-wrench"></i> Cambiar Contraseña</a>
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="fa fa-lock"></i>
                Cerrar Sesión
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
            </a>


          </div>
        </li>
      </ul>

    </header>
    <div class="app-body">
      @include('layouts.menulateral')
      <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb oculto-impresion">
          <li class="breadcrumb-item text-success">
            @yield('seccion')
          </li>
        </ol>
        <div class="container-fluid">
          <div class="animated fadeIn">

            <!-- /.row-->
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <h4 class="card-title mb-0">@yield('titulo')</h4>
                    <div class="small text-muted">@yield('descripcion')</div>
                  </div>
                  <div class="col-sm-6 text-right" >
                    <h4 class="card-title mb-0">@yield('boton')</h4>
                  </div>

                  <!-- /.col-->
                  <div class="col-sm-7 d-none d-md-block">
                    @yield('opciones')
                  </div>
                  <!-- /.col-->
                </div>
                @if (Session::has('info'))
                  <div style="margin-top:15px;" class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{Session::get('info')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif

                @if ($errors->any())
                  <div style="margin-top:15px;" class="alert alert-danger alert-dismissible fade show" role="alert">
                    @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                    @endforeach
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif
                <!-- /.row-->
                <div  style="margin-top:15px;">

                  @yield('contenido')

                </div>
              </div>
              <!--<div class="card-footer">
                <div class="row text-center">
                </div>
              </div>-->
            </div>
          </div>
        </div>
      </main>
    </div>
  </body>
