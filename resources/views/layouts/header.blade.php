<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.15
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>DAGISA: SISTEMA OPERATIVO</title>
    <!-- Icons-->
    <link rel="icon" type="image/ico" href="{{asset('pan/img/brand/sygnet.ico')}}" sizes="any" />
    <link href="{{asset('pan/node_modules/@coreui/icons/css/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{asset('pan/node_modules/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet">
    <link href="{{asset('pan/node_modules/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('pan/node_modules/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('pan/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('pan/vendors/pace-progress/css/pace.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href=" {{asset('pan/css/jquery-ui.theme.min.css')}} ">
    <link rel="stylesheet" type="text/css" href=" {{asset('pan/css/jquery-ui.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('pan/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" >

    <!-- Global site tag (gtag.js) - Google Analytics-->

    <script src="{{asset('pan/node_modules/jquery/dist/jquery-1.12.4.js')}}"></script>

    <script src="{{asset('/js/gijgo.min.js')}}" type="text/javascript"></script>
    <link href=" {{asset('css/gijgo.min.css')}}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" charset="utf-8"></script>


<!--    <script src="{{asset('pan/node_modules/jquery/dist/jquery.min.js')}}"></script>-->

    <script src="{{asset('pan/node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('pan/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('pan/node_modules/pace-progress/pace.min.js')}}"></script>
    <script src="{{asset('pan/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('pan/node_modules/@coreui/coreui/dist/js/coreui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('pan/js/jquery-ui.min.js')}}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
    </head>
