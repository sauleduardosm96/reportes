@extends('layouts.main')

@section('seccion')
  Clientes
@endsection

@section('titulo')
  Gestor de clientes
@endsection

@section('boton')
  <a href="{{route('clientes.create')}}" class="btn  btn-info text-white"><i class="fa fa-plus"></i> Registrar nuevo cliente</a>
@endsection

@section('descripcion')
  Panel de control para gestionar los clientes
@endsection


@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection


@section('contenido')
{{Form::open(['route'=>'clientes.index','method'=>'GET'])}}
  @include('layouts.busqueda')
{{Form::close()}}
  <table class="table table-hover table-outline text-center">
    <thead class="thead-light">
      <tr>
        <th scope="col">Código</th>
        <th scope="col">Cliente</th>
        <th scope="col">Dirección</th>
        <th scope="col">Negocio</th>
        <th scope="col">Teléfono</th>
        <th scope="col">Correo</th>
        <th scope="col">Editar</th>
      </tr>
    </thead>
    <tbody>
      @foreach($clientes as $c)
        <tr>
          <th scope="row">{{$c->id}}</th>
          <td>{{$c->nombre}}</td>
          <td>{{$c->direccion}}</td>
          <td>{{$c->negocio}}</td>
          <td>{{$c->telefono}}</td>
          <td>{{$c->correo}}</td>
          <td>
            <a class="btn  btn-info text-white" href="{{route('clientes.edit',$c->id)}}"><i class="icon-pencil "></i></a>
          </td>
        </tr>
      @endforeach

    </tbody>
  </table>
  {!!$clientes->appends(Request::only(['nombre']))->render()!!}
  <script type="text/javascript">
    function cerrar() {
        $('#not').remove();
    }
  </script>

@endsection
