@extends('layouts.main')

@section('seccion')
  Pagos / Registrar
@endsection

@section('titulo')
  Remisión: {{$remision->idre}} <br>
  Cliente: {{$remision->nombre}} <br>
  Total de la remisión: ${{number_format($remision->importe,2)}}
@endsection

@section('descripcion')
  Añadir pago a la remisión
@endsection



@section('opciones')
  <!--<button class="btn btn-success float-right" type="button">
    <i class="icon-cloud-download"></i>
  </button>
  <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
    <label class="btn btn-outline-secondary">
      <input id="option1" type="radio" name="options" autocomplete="off"> Day
    </label>
    <label class="btn btn-outline-secondary active">
      <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
    </label>
    <label class="btn btn-outline-secondary">
      <input id="option3" type="radio" name="options" autocomplete="off"> Year
    </label>
  </div>-->
@endsection

@section('contenido')
<table class="table table-hover table-outline text-center">
  <thead class="thead-light">
        <tr>
          <th scope="col">Fecha</th>
          <th scope="col">Descripcion</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Quitar</th>
        </tr>
      </thead>
      <tbody>
        @php
          $cont=0;
        @endphp
        @foreach($pagos as $p)
          <tr>
            <td>{{$p->fecha}}</td>
            <td>{{$p->descripcion}}</td>
            <th scope="row">${{number_format($p->cantidad,2)}}</th>
            <td>
              {!! Form::open(['route'=>['pagos.destroy', $p->id], 'method'=>'DELETE', 'id'=>'formDes','onsubmit'=>'return confirm("¿Seguro que desea eliminarlo?")']) !!}
                <button type="submit" class="btn-info btn text-white" name="button"><i class="fa fa-trash "></i></button>
                <input type="hidden" name="val" value="{{$remision->idre}}">
              {!!Form::close()!!}
            </td>
            @php
            $cont=$cont+$p->cantidad;
            @endphp
          </tr>
        @endforeach
        <tr>
          <td>

          </td>
          <th>
            Total pagado:
          </th>
          <th scope="row">
            ${{number_format($cont,2)}}
          </th>
          <td>

          </td>
        </tr>
        <tr>
          <td>

          </td>
          <th>
            Pendiente de pago:
          </th>
          <th scope="row">
            ${{number_format($remision->importe-$cont,2)}}
          </th>
          <td>

          </td>
        </tr>
      </tbody>
    </table>
    <br><br>
    <h4>Añadir nuevo pago</h4>
    <form action="{{route('pagos.update',$remision->idre)}}"  method="POST">
      <input type="hidden" name="_method" value="PUT">
      <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
      <div class="form-group">
        <label for="cantidad">Cantidad</label>
        <input type="number"  required step="0.01" name="cantidad" id="cantidad" class="form-control">
      </div>

      <div class="form-group">
        <label for="descripcion">Descripción</label>
        <input type="text" required name="descripcion" id="descripcion" class="form-control">
      </div>
      <div class="form-group">
        <label for="fecha">Fecha</label>
        <div class="col-2">
          <input type="date" required name="fecha" id="fecha" class="form-control">
        </div>
      </div>



      <div class="d-flex">
        <div class="mr-auto p-2">
          <button type="submit" class="btn  btn-info text-white">Guardar</button>
          <a href="{{route('remisiones.index')}}" class="btn  btn-secondary">Regresar</a>
        </div>
      </div>
    </form>
@endsection
