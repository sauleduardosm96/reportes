@include('layouts.header')

  <body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="text-center">
                <img class="navbar-brand-full text-center" src="{{asset('pan/img/brand/logo.png')}}" width="150"  alt="SICA">
              </div>
              <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                  @csrf
                  <h1>Iniciar Sesión</h1>
                  <p class="text-muted">Inicia sesión con tu cuenta</p>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="icon-user"></i>
                      </span>
                    </div>

                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Cuenta">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="icon-lock"></i>
                      </span>
                    </div>

                    <input id="password" type="password" placeholder="Contraseña" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" autocomplete="current-password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="row">
                    <div class="col-6">
                      <button class="btn btn-info text-white px-4" type="submit">Iniciar Sesión</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="card text-white bg-info py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
		  <h2 style="margin-bottom:7%">Obtener registro</h2>
                  <p>Para tener acceso a DAGISA es necesario que acudas con el administrador del sistema y solicites una cuenta.</p>
                  <h2 style="margin-bottom:7%">Recuperar contraseña</h2>
                  <p>Si olvidaste tu contraseña puedes acudir con el administrador del sistema para reasignar una nueva.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
  </body>
</html>
