<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect()->route('clientes.index');
});

Route::get('/home', function(){
	return redirect()->route('clientes.index');
})->name('home');

Route::post('/contacto','ContactoController@contacto')->name('contacto');

Route::resource('/tickets','TicketsController')->middleware('auth');

Route::resource('/remisiones','RemisionesController')->middleware('auth');
Route::resource('/compras','ComprasController')->middleware('auth');

Route::resource('/clientes','ClientesController')->middleware('auth');
Route::resource('/proveedores','ProveedoresController')->middleware('auth');

Route::resource('/laboratorios','LaboratoriosController')->middleware('auth');
Route::resource('/medicamentos','MedicamentosController')->middleware('auth');
Route::resource('/rutas','RutasController')->middleware('auth');

Route::post('/reporteClientes','ReportesController@reporteClientes')->name('reporteClientes')->middleware('auth');
Route::post('/reporteProveedores','ReportesController@reporteProveedores')->name('reporteProveedores')->middleware('auth');


Route::get('/ticketsAs','TicketsController@ticketsAs')->name('ticketsAs');
Route::get('/provsAs','TicketsController@provsAs')->name('provsAs');
Route::get('/rutasAs','RutasController@rutasAs')->name('rutasAs');
Route::get('/ticketAs','TicketsController@ticketAs')->name('ticketAs');
Route::get('/ticketPAs','TicketsController@ticketPAs')->name('ticketPAs');
Route::get('/laboratorioAs','LaboratoriosController@laboratorioAs')->name('laboratorioAs');
Route::get('/descripcionAs','RemisionesController@descripcionAs')->name('descripcionAs');

Route::get('/cambiar', 'UsuariosController@cambiar')->name('cambiar')->middleware('auth');
Route::post('/cambio', 'UsuariosController@cambio')->name('cambio')->middleware('auth');
Route::resource('/usuarios','UsuariosController')->middleware('auth');

Route::resource('/pagos','PagosController')->middleware('auth');
Route::resource('/abonos','AbonosController')->middleware('auth');

Route::get('/catalogos', 'MedicamentosController@catalogos')->name('catalogos')->middleware('auth');
